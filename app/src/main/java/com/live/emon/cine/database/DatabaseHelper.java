package com.live.emon.cine.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "cinematic_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(TimeTracker.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TimeTracker.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    /*public long insertContentId(String contentId) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(TimeTracker.CONTENT_ID, contentId);

        // insert row
        long id = db.insert(TimeTracker.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }*/


    public long insertTime(TimeTracker timeTracker) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(TimeTracker.CONTENT_ID, timeTracker.getContentId());
        values.put(TimeTracker.CONTENT_TIME, timeTracker.getTime());

        // insert row
        long id = db.insert(TimeTracker.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public TimeTracker getTime(String contentID) {

        if (getCount() == 0)
            return null;
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TimeTracker.TABLE_NAME,
                new String[]{TimeTracker.CONTENT_ID, TimeTracker.CONTENT_TIME},
                TimeTracker.CONTENT_ID + "=?",
                new String[]{String.valueOf(contentID)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        assert cursor != null;
        TimeTracker note = new TimeTracker(
                cursor.getString(cursor.getColumnIndex(TimeTracker.CONTENT_ID)),
                cursor.getInt(cursor.getColumnIndex(TimeTracker.CONTENT_TIME)));

        // close the db connection
        cursor.close();

        return note;
    }

    public List<TimeTracker> getAllNotes() {
        List<TimeTracker> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TimeTracker.TABLE_NAME + " ORDER BY " +
                TimeTracker.CONTENT_TIME + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TimeTracker note = new TimeTracker();
                note.setContentId(cursor.getString(cursor.getColumnIndex(TimeTracker.CONTENT_ID)));
                note.setTime(cursor.getInt(cursor.getColumnIndex(TimeTracker.CONTENT_TIME)));

                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    public int getCount() {
        String countQuery = "SELECT  * FROM " + TimeTracker.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateNote(TimeTracker note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TimeTracker.CONTENT_ID, note.getContentId());
        values.put(TimeTracker.CONTENT_TIME, note.getTime());

        // updating row
        return db.update(TimeTracker.TABLE_NAME, values, TimeTracker.CONTENT_ID + " = ?",
                new String[]{String.valueOf(note.getContentId())});
    }

    public void deleteNote(TimeTracker note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TimeTracker.TABLE_NAME, TimeTracker.CONTENT_ID + " = ?",
                new String[]{String.valueOf(note.getContentId())});
        db.close();
    }
}