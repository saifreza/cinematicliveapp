package com.live.emon.cine.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.live.emon.cine.R;
import com.live.emon.cine.activity.PlayerActivity;
import com.live.emon.cine.utils.Constant;


public class CinematicOfflineFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ImageView imageView;

    public CinematicOfflineFragment() {
        // Required empty public constructor
    }

    public static CinematicOfflineFragment newInstance(String param1, String param2) {
        CinematicOfflineFragment fragment = new CinematicOfflineFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_free_movies, container, false);
        imageView = view.findViewById(R.id.imv_rongbaz);
        imageView.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void goToPlayerActivity() {


        Intent intent = new Intent(getActivity(), PlayerActivity.class);
        intent.putExtra(Constant.CONTENT_URL, "http://192.168.0.100/cinematic/bhaijaan.mp4");

        startActivity(intent);


    }


    @Override
    public void onClick(View v) {
        //goToPlayerActivity();
    }
}
