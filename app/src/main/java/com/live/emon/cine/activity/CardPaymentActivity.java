package com.live.emon.cine.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.live.emon.cine.R;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.SharedPref;

public class CardPaymentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
    }
}
