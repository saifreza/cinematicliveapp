package com.live.emon.cine.network;


import com.live.emon.cine.model.*;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface ApiInterface {
    @GET("content")
    Call<ItemResponse> getItemsByCategory(@Query("category") String category);

    @GET("details")
    Call<ItemResponse> getItem(@Query("contentId") String contentId);

    @GET("category")
    Call<CategoryResponse> getCategories();


    @GET("topcontent")
    Call<ItemResponse> getTopContent();

    @GET("search")
    Call<ItemResponse> searchContent(@Query("searchKey") String searchKey);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("useractivity")
    Call<String> sendUserActivity(@Query("userId") String userId,
                                  @Query("contentId") String contentId,
                                  @Query("category") String category,
                                  @Query("contentName") String contentName,
                                  @Query("channel") String channel);


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("registration")
    Call<String> sendUserInfo(@Query("deviceId") String deviceId,
                              @Query("mobileNo") String mobileNo,
                              @Query("op") String op);


    @GET("get_mobileno.php")
    Call<OperatorInfo> sendOperatorInfo();


    @GET("registration")
    Call<String> getUserInfo(
            @Query("mobileNo") String mobileNo);


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("watchlater")
    Call<String> sendWatchList(@Query("userId") String userId,
                               @Query("contentId") String contentId,
                               @Query("thumbnail") String thumbnail,
                               @Query("watchState") String watchState,
                               @Query("premium") String premium
    );

    @GET("watchlater")
    Call<WatchListResponseResponse> getMyWatchList(@Query("userId") String userId);

    @GET("watchstate")
    Call<String> getWatchState(@Query("userId") String userId, @Query("contentId") String contentId);


    @GET("billing")
    Call<BillingItemResponse> getBillingInfo(
            @Query("op") String op,
            @Query("mobileNo") String mobileNo,
            @Query("channel") String channel);

    @GET("unsub")
    Call<BillingItemResponse> getUnSubInfo(
            @Query("op") String op,
            @Query("mobileNo") String mobileNo,
            @Query("channel") String channel);

    @GET("subscribe")
    Call<SubResponse> isSubscribe(
            @Query("mobileNo") String mobileNo,
            @Query("op") String op);


    @GET
    Call<String> userSubscription(@Url String url);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("otpvalidation")
    Call<String> otpValidation(@Query("deviceId") String deviceId,
                               @Query("mobileNo") String mobileNo,
                               @Query("otp") String otp
    );


    @GET("help")
    Call<HelpResponse> getHelp(@Query("op") String op);


    @GET("songlist")
    Call<ItemResponse> getSongList(@Query("album") String album);

    @GET("continue-watching")
    Call<ContinueWatchItemResponse> getContinueWatchMovies(@Query("userId") String userId);

    @POST("continue-watching")
    Call<String> sendContinueWatchMovie(@Query("userId") String userId,
                                        @Query("contentId") String contentId,
                                        @Query("playTime") String playTime,
                                        @Query("thumbnail") String thumbnail,
                                        @Query("contentLength") String contentLength,
                                        @Query("playPercentDuration") String playPercentDuration,
                                        @Query("premium") String premium );

}
