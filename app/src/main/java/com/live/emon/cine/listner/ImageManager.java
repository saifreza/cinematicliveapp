package com.live.emon.cine.listner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.live.emon.cine.utils.Constant;

public class ImageManager {
    private static ImageManager ourInstance;
    private DataListener listener;
    private Context mContext;

  public static ImageManager getInstance(Context context) {
        if (ourInstance == null)
            return new ImageManager(context);
        else
            return ourInstance;
    }

    private ImageManager(Context context) {
        this.listener = null;
        this.mContext = context;
        loadBannerImage();
    }

    public void setImageListener(DataListener listener) {
        this.listener = listener;
    }

    public void deactivedListener() {
        this.listener = null;
    }

    private void loadBannerImage() {
        Glide.with(mContext)
                .load(Constant.REG_URL)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(false)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        Drawable drawable = new BitmapDrawable(mContext.getResources(), resource);
                        if (listener != null)
                            listener.loadImage(drawable);

                    }
                });
    }

}
