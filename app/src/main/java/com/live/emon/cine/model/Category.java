package com.live.emon.cine.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category extends Base {

    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("catId")
    @Expose
    private String catId;

    @SerializedName("status")
    @Expose
    private Integer status;

    private List<Item> arrayList;


    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Category() {
    }

    /*@Override
    public boolean equals(Object inObject) {
        if (inObject == null || getClass() != inObject.getClass()) return false;
        if (inObject instanceof Category) {
            Category item = (Category) inObject;
            return item.getCategory().equals(item.getCategory()) ;
        }
        return false;
    }*/

    private Category(Parcel in) {
        category = in.readString();
        catId = in.readString();
        status = in.readInt();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(category);
        dest.writeString(catId);
        dest.writeInt(status);

    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Item> getArrayList() {
        return arrayList;
    }

    public void setArrayList(List<Item> arrayList) {
        this.arrayList = arrayList;
    }




}
