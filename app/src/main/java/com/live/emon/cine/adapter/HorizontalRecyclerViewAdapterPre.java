package com.live.emon.cine.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.live.emon.cine.R;
import com.live.emon.cine.activity.DetailActivity;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.Glider;

import java.util.List;


public class HorizontalRecyclerViewAdapterPre extends
        RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Item> mArrayList;
    private static final int CONTINUE_WATCHING_MOVIE = 1;
    private static final int LATEST_MOVIE = 0;

    public HorizontalRecyclerViewAdapterPre(Context mContext,
                                            List<Item> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    public int getImage(String imageName) {

        return mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       /* View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_horizontal, parent, false);
        return new HorizontalViewHolder(view);
*/

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case CONTINUE_WATCHING_MOVIE:
                View viewItem = inflater.inflate(R.layout.item_continue_watch, parent, false);
                viewHolder = new ContinueWatchViewHolder(viewItem);
                break;
            case LATEST_MOVIE:
                View viewLoading = inflater.inflate(R.layout.item_horizontal, parent, false);
                viewHolder = new HorizontalViewHolder(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Item item = mArrayList.get(position);
        switch (getItemViewType(position)) {

            case LATEST_MOVIE:
                final HorizontalViewHolder heroVh = (HorizontalViewHolder) holder;
                heroVh.cardView.setTag(item);

                if (item.getPremium().equalsIgnoreCase("1")) {
                    heroVh.tvPremium.setVisibility(View.VISIBLE);
                } else {
                    heroVh.tvPremium.setVisibility(View.GONE);
                }
                Glider.showWithPlaceholder(heroVh.imageView, item.getThumbnail());
                heroVh.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mContext, item.getPortrait(), Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case CONTINUE_WATCHING_MOVIE:
                final Item continueWatchMovie = mArrayList.get(position);

                ContinueWatchViewHolder loadingVH = (ContinueWatchViewHolder) holder;
                break;
        }
    }


    @Override
    public int getItemCount() {
        if (mArrayList != null) {
            return mArrayList.size();
        } else
            return 0;

    }

    class HorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // TextView txtTitle;
        private ImageView imageView;
        private TextView tvPremium;
        private CardView cardView;

        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/the real magazine demo.ttf");
        ;


        public HorizontalViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivThumb);
            tvPremium = itemView.findViewById(R.id.tv_premium);
            cardView = itemView.findViewById(R.id.lyt_main);
            cardView.setOnClickListener(this);
            /*tvPremium.setTypeface(font);
            tvPremium.setText("Premium");
            tvPremium.setTextColor(mContext.
                    getResources().
                    getColor(R.color.black));*/
            // itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Item item = (Item) v.getTag();
            if (item == null)
                return;
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra(Constant.CONTENT_ID, item.getContentId());
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        }
    }


    class ContinueWatchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // TextView txtTitle;
        private ImageView imageView;
        private TextView tvPremium;
        private CardView cardView;

        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/the real magazine demo.ttf");
        ;


        public ContinueWatchViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivThumb);
            tvPremium = itemView.findViewById(R.id.tv_premium);
            cardView = itemView.findViewById(R.id.lyt_main);
            cardView.setOnClickListener(this);
            /*tvPremium.setTypeface(font);
            tvPremium.setText("Premium");
            tvPremium.setTextColor(mContext.
                    getResources().
                    getColor(R.color.black));*/
            // itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Item item = (Item) v.getTag();
            if (item == null)
                return;
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra(Constant.CONTENT_ID, item.getContentId());
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return LATEST_MOVIE;
        } else {
            return CONTINUE_WATCHING_MOVIE;
        }
    }
}
