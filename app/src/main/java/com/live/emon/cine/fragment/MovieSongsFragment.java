package com.live.emon.cine.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.emon.cine.R;
import com.live.emon.cine.adapter.MoviesAdapter;
import com.live.emon.cine.helper.MyDividerItemDecoration;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class MovieSongsFragment extends Fragment {

    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private View view;
    private static final String ARG_PARAM1 = "param1";

    public MovieSongsFragment() {
        // Required empty public constructor
    }


    public static MovieSongsFragment newInstance(String mParam) {
        MovieSongsFragment fragment = new MovieSongsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, mParam);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_trailer, container, false);
        intiViw();
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();

        if (getArguments() != null) {
            String mContentName = getArguments().getString(ARG_PARAM1);
            if (!TextUtils.isEmpty(mContentName))
                getSongList(mContentName.trim());
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void intiViw() {
        recyclerView =  view.findViewById(R.id.recy_view_coming_soon);

        mAdapter = new MoviesAdapter(getActivity(), movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new MyDividerItemDecoration(2,
                Objects.requireNonNull(getActivity()).getResources().getColor(R.color.border_color)));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);
    }


    private void getSongList(String album) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ItemResponse> responseCall = apiService.getSongList(album);
        responseCall.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() == null)
                        return;
                    movieList = response.body().getItems();
                    Timber.e(movieList.toString());
                    mAdapter = new MoviesAdapter(getActivity(), movieList);
                    recyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ItemResponse> call, @NonNull Throwable t) {

            }
        });
    }
}
