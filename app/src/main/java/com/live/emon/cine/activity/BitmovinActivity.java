package com.live.emon.cine.activity;/*
package cine.emon.live.com.cinematic.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.bitmovin.player.BitmovinPlayer;
import com.bitmovin.player.BitmovinPlayerView;
import com.bitmovin.player.config.drm.DRMSystems;
import com.bitmovin.player.config.media.SourceConfiguration;
import com.bitmovin.player.config.media.SourceItem;

import java.util.UUID;

import cine.emon.live.com.cinematic.R;

public class BitmovinActivity extends AppCompatActivity {


    private BitmovinPlayerView bitmovinPlayerView;
    private BitmovinPlayer bitmovinPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_bitmovin);
        this.bitmovinPlayerView = (BitmovinPlayerView) this.findViewById(R.id.bitmovinPlayerView);
        this.bitmovinPlayer = this.bitmovinPlayerView.getPlayer();

        this.initializePlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.bitmovinPlayerView.onResume();
    }

    @Override
    protected void onPause() {
        this.bitmovinPlayerView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        this.bitmovinPlayerView.onDestroy();
        super.onDestroy();
    }

    protected void initializePlayer() {
        // Create a new source configuration
        SourceConfiguration sourceConfiguration = new SourceConfiguration();

        // Create a new source item
        SourceItem sourceItem = new SourceItem("https://s3-ap-southeast-1.amazonaws.com/cinematic-vod/boom/stream.mpd");

        // setup DRM handling
        String drmLicenseUrl = "https://wv.service.expressplay.com/hms/wv/rights/?ExpressPlayToken=BAAUGMF0KcEAAABgju7bQb0dssr4qkVm0bKmJFgSqS8cxPR7DsaH8NqGrUqCYNzdSMxiczv13FvU_h2etOGMGquPfsttYZ5YE_emaloqSoHhOzMd18U9WPxpjCVhRuMTdh8th5nZFmYkTNjIKltadO6ApgUX337HD0DDVqGzqvk";
        UUID drmSchemeUuid = DRMSystems.WIDEVINE_UUID;
        sourceItem.addDRMConfiguration(drmSchemeUuid, drmLicenseUrl);

        // Add source item including DRM configuration to source configuration
        sourceConfiguration.addSourceItem(sourceItem);

        // load source using the created source configuration
        this.bitmovinPlayer.load(sourceConfiguration);
  // Create a new source configuration
     */
/* SourceConfiguration sourceConfiguration = new SourceConfiguration();

        // Add a new source item
        sourceConfiguration.addSourceItem("https://s3-ap-southeast-1.amazonaws.com/cinematic-vod/boom/stream.mpd");

        // load source using the created source configuration
        this.bitmovinPlayer.load(sourceConfiguration);
*//*

    }
}
*/
