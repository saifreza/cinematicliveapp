package com.live.emon.cine.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.live.emon.cine.fragment.SimilarFragment;
import com.live.emon.cine.fragment.MovieSongsFragment;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private String mArtistName;
    private String mAlbamName;

    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    public ViewPagerAdapter(Context context, FragmentManager fm, String param, String albamName) {
        super(fm);
        mContext = context;
        mArtistName = param;
        mAlbamName = albamName;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return SimilarFragment.newInstance(mArtistName);
        } else {
            return MovieSongsFragment.newInstance(mAlbamName);
        }

    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        //make it two
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "MORE LIKE THIS";
            case 1:
                return "MOVIE SONGS";
            default:
                return null;
        }
    }

}