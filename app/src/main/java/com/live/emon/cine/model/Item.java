package com.live.emon.cine.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.common.internal.Objects;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item extends Base {

    @SerializedName("portrait")
    @Expose
    private String portrait;
    @SerializedName("artist")
    @Expose
    private String artist;
    @SerializedName("createdate")
    @Expose
    private String createdate;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("royality")
    @Expose
    private String royality;
    @SerializedName("subcategory")
    @Expose
    private String subcategory;
    @SerializedName("album")
    @Expose
    private String album;
    @SerializedName("length")
    @Expose
    private String length;
    @SerializedName("landscape")
    @Expose
    private String landscape;
    @SerializedName("territory")
    @Expose
    private String territory;
    @SerializedName("contentId")
    @Expose
    private String contentId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("director")
    @Expose
    private String director;


    @SerializedName("watchState")
    @Expose
    private String watchState;

    @SerializedName("premium")
    @Expose
    private String premium;

    @SerializedName("freePlayTime")
    @Expose
    private String freePlayTime;


    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;


    private String playTime;

    private String userid;


    private String playPercentDuration;


    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Item() {
    }

    public Item(Parcel in) {
        portrait = in.readString();
        artist = in.readString();
        createdate = in.readString();
        file = in.readString();
        royality = in.readString();
        subcategory = in.readString();
        album = in.readString();
        length = in.readString();
        landscape = in.readString();
        territory = in.readString();
        contentId = in.readString();
        year = in.readString();
        category = in.readString();
        tag = in.readString();
        description = in.readString();
        title = in.readString();
        type = in.readString();
        director = in.readString();
        watchState = in.readString();
        premium = in.readString();
        freePlayTime = in.readString();
        thumbnail = in.readString();
        playTime = in.readString();
        userid = in.readString();
        playPercentDuration = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(portrait);
        dest.writeString(artist);
        dest.writeString(createdate);
        dest.writeString(file);
        dest.writeString(royality);
        dest.writeString(subcategory);
        dest.writeString(album);
        dest.writeString(length);
        dest.writeString(landscape);
        dest.writeString(territory);
        dest.writeString(contentId);
        dest.writeString(year);
        dest.writeString(category);
        dest.writeString(tag);
        dest.writeString(description);
        dest.writeString(title);
        dest.writeString(type);
        dest.writeString(director);
        dest.writeString(watchState);
        dest.writeString(premium);
        dest.writeString(freePlayTime);
        dest.writeString(thumbnail);
        dest.writeString(playTime);
        dest.writeString(userid);
        dest.writeString(playPercentDuration);

    }

   /* @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Item item = (Item) obj;
        return Objects.equal(item, item.item);
    }*/

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getRoyality() {
        return royality;
    }

    public void setRoyality(String royality) {
        this.royality = royality;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getLandscape() {
        return landscape;
    }

    public void setLandscape(String landscape) {
        this.landscape = landscape;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWatchState() {
        return watchState;
    }

    public void setWatchState(String watchState) {
        this.watchState = watchState;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }


    public String getFreePlayTime() {
        return freePlayTime;
    }

    public void setFreePlayTime(String freePlayTime) {
        this.freePlayTime = freePlayTime;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPlayPercentDuration() {
        return playPercentDuration;
    }

    public void setPlayPercentDuration(String playPercentDuration) {
        this.playPercentDuration = playPercentDuration;
    }
}