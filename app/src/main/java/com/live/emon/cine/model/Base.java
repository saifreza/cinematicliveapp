package com.live.emon.cine.model;

import android.os.Parcel;

public abstract class Base extends BaseParcel {

    protected long time;

    protected Base() {
    }

    protected Base(Parcel in) {
        super(in);
        time = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(time);
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }
}
