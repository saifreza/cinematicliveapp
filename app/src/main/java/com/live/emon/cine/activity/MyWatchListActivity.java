package com.live.emon.cine.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.WatchListAdapter;
import com.live.emon.cine.model.WatchItem;
import com.live.emon.cine.model.WatchListResponseResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MyWatchListActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private WatchListAdapter watchListAdapter;
    private List<WatchItem> watchItems;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private ImageButton imgBtnBack;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_watch_list);
        initToolbar();
        initView();
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        /*Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);*/
        tvToolbarFont.setText(getString(R.string.my_list));
    }

    private void initView() {

        relativeLytNoNetwork = (RelativeLayout) findViewById(R.id.relative_lyt_no_network);
        imgNoNetLogo = (ImageView) findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = (TextView) findViewById(R.id.tv_something_wrong);
        tvfailedMessage = (TextView) findViewById(R.id.failed_message);


        btnFailedRetry = (Button) findViewById(R.id.btn_failed_retry);
        btnFailedRetry.setOnClickListener(this);


        imgBtnBack = findViewById(R.id.img_btn_back);
        imgBtnBack.setOnClickListener(this);

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        int myColor = Color.parseColor("#494848");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.BLACK, Color.GRAY);

        watchItems = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        watchListAdapter = new WatchListAdapter(this, watchItems);
        recyclerView.setAdapter(watchListAdapter); // set the Adapter to RecyclerView

    }

    private void loadData(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                swipeRefresh.setRefreshing(true);
                loadMyWatchList(SharedPref.read(Constant.USER_PHONE_NO));
            }
        } else {
            swipeRefresh.setRefreshing(false);
             relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });
    }


    private void loadMyWatchList(String userId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<WatchListResponseResponse> call = apiService.getMyWatchList(userId);
        call.enqueue(new Callback<WatchListResponseResponse>() {
            @Override
            public void onResponse(@NonNull Call<WatchListResponseResponse> call, Response<WatchListResponseResponse> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    List<WatchItem> itemList = response.body().getItems();
                    if (itemList != null) {
                        swipeRefresh.setRefreshing(false);
                        watchListAdapter.clear();
                        Timber.e(watchItems.toString());
                        for (int i = 0; i < itemList.size(); i++) {
                            if (itemList.get(i).getWatchStatus().equals("1")) {
                                // watchListAdapter.clear();
                                if (!watchItems.contains(itemList.get(i))) {
                                    watchItems.add(itemList.get(i));
                                    watchListAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                    }
                }
                swipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<WatchListResponseResponse> call, Throwable t) {
                  swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_back:
                onBackPressed();
                break;
            case R.id.btn_failed_retry:
                Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
                    @Override
                    public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                        loadData(isConnected);
                    }
                });
                break;
            default:
                break;
        }
    }
}
