package com.live.emon.cine.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;


import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.emon.cine.R;
import com.live.emon.cine.model.BillingItem;
import com.live.emon.cine.model.BillingItemResponse;
import com.live.emon.cine.model.OperatorInfo;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.Glider;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SubscriptionActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivOperator;
    private Button btnCardPayment, btnMobilePayment;
    private String subApi = "";
    private String loadSubApi = "";
    private String apiMessage = "";
    private String apiBilling = "";
    private String messageBody = "";
    private String billingAmount = "";
    private FirebaseAnalytics mFirebaseAnalytics;
    private ProgressBar progressBar, progressBarWeb;
    private int count = 0;
    private WebView webView;
    private LinearLayout lytMobileBilling;
    private LinearLayout lytWebView;
    private TextView tvPaymentBangla, tvPaymentEnglish;
    private ImageView ivBanner;
    private ImageButton imgBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_new);
        initToolbar();
        initView();
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


    }

   /* public void setExternalFonts(EditText tv) {
        Typeface tf = Typeface.createFromAsset(this.getAssets(),
                "fonts/SUTOM___.TTF");
        tv.setTypeface(tf);
        tv.setHint(this.getResources().getString(R.string.pay_bill_info));
    }*/

    private String getActivityTag() {
        return getIntent().getStringExtra(Constant.ACTIVITY_TAG);
    }

    private String getDirectSub() {
        return getIntent().getStringExtra(Constant.DIRECT_SUB);
    }


    private void checkBlDataConnection() {


        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {

                            return;
                        } else {
                            if (operatorInfo.getOp().equalsIgnoreCase("BDB111")) {
                                getBillingInfo(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));

                            } else {
                                tvPaymentBangla.setText("Please Try From Baglalink Mobile Data");
                                tvPaymentEnglish.setVisibility(View.GONE);
                                btnMobilePayment.setVisibility(View.GONE);

                            }

                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(SubscriptionActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }


    private void checkGpDataConnection() {


        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {

                            return;
                        } else {
                            if (operatorInfo.getOp().equalsIgnoreCase("BDB111")) {
                                getBillingInfo(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));

                            } else {
                                tvPaymentBangla.setText("Please Try From Gp Mobile Data");
                                tvPaymentEnglish.setVisibility(View.GONE);
                                btnMobilePayment.setVisibility(View.GONE);

                            }

                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(SubscriptionActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }


    private void initWebView(String url) {
        webView = (WebView) findViewById(R.id.webView);

        webView.clearCache(true);
        webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        progressBarWeb.setVisibility(View.VISIBLE);
        progressBarWeb.setProgress(100);

        webView.loadUrl(url);
    }


    private void initGpWebView(String url) {
        webView = (WebView) findViewById(R.id.webView);

        webView.clearCache(true);
        webView.setWebViewClient(new MyGPBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        progressBarWeb.setVisibility(View.VISIBLE);
        progressBarWeb.setProgress(100);

        webView.loadUrl(url);
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBarWeb.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            String unSubUrl = url;
            unSubUrl = unSubUrl.substring(0, 29);
            if (!TextUtils.isEmpty(unSubUrl) && unSubUrl.equalsIgnoreCase(
                    "http://cinematic.mobi/app.php")) {

                if (url.contains("isChargeSuccess=1001") || url.contains("isChargeSuccess=0")) {
                    SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                }
                Intent intent;
                if (TextUtils.isEmpty(getActivityTag())) {
                    //intent = new Intent(SubscriptionActivity.this, PlayerActivity.class);
                    intent = new Intent(SubscriptionActivity.this, DetailActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, getUrl());
                    intent.putExtra(Constant.CONTENT_TYPE, getContentType());
                    intent.putExtra(Constant.CONTENT_ID, getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, isPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, getContentName());
                    Toast.makeText(SubscriptionActivity.this, "Request Accepted", Toast.LENGTH_SHORT).show();

                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                } else {
                    intent = new Intent(SubscriptionActivity.this, MainActivity.class);

                }
                startActivity(intent);
                finish();

            }
        }
    }


    private class MyGPBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBarWeb.setVisibility(View.GONE);
            super.onPageFinished(view, url);

            if (url.contains("successSTATUS=1") || url.contains("successSTATUS=0") ||
                    url.contains("successSTATUS=2")) {
                Intent intent;
                if (url.contains("successSTATUS=1")) {
                    SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                }
                if (TextUtils.isEmpty(getActivityTag())) {
                    //intent = new Intent(SubscriptionActivity.this, PlayerActivity.class);
                    intent = new Intent(SubscriptionActivity.this, DetailActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, getUrl());
                    intent.putExtra(Constant.CONTENT_TYPE, getContentType());
                    intent.putExtra(Constant.CONTENT_ID, getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, isPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, getContentName());
                    Toast.makeText(SubscriptionActivity.this, "Request Accepted", Toast.LENGTH_SHORT).show();

                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                } else {
                    intent = new Intent(SubscriptionActivity.this, MainActivity.class);

                }
                startActivity(intent);
                finish();

            }

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            String unSubUrl = url;
            unSubUrl = unSubUrl.substring(0, 29);


            if (!TextUtils.isEmpty(unSubUrl) && unSubUrl.equalsIgnoreCase(
                    "http://cinematic.mobi/app.php")) {

                if (url.contains("isChargeSuccess=1001") || url.contains("isChargeSuccess=0")) {
                    SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                }

                Intent intent;
                if (TextUtils.isEmpty(getActivityTag())) {
                    //intent = new Intent(SubscriptionActivity.this, PlayerActivity.class);
                    intent = new Intent(SubscriptionActivity.this, DetailActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, getUrl());
                    intent.putExtra(Constant.CONTENT_TYPE, getContentType());
                    intent.putExtra(Constant.CONTENT_ID, getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, isPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, getContentName());
                    Toast.makeText(SubscriptionActivity.this, "Request Accepted", Toast.LENGTH_SHORT).show();

                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                } else {
                    intent = new Intent(SubscriptionActivity.this, MainActivity.class);

                }
                startActivity(intent);
                finish();

            }
        }
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            // getSupportActionBar().setDisplayShowTitleEnabled(false);
            //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }
        tvToolbarFont.setTypeface(font);
    }

    private void initView() {
        ivOperator = (ImageView) findViewById(R.id.iv_operator);
        ivBanner = (ImageView) findViewById(R.id.iv_banner);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        progressBarWeb = (ProgressBar) findViewById(R.id.progress_bar_web);
        progressBarWeb.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);


        // btnCardPayment = (Button) findViewById(R.id.btn_card_payment);
        lytMobileBilling = (LinearLayout) findViewById(R.id.lyt_mobile_billing);
        lytWebView = (LinearLayout) findViewById(R.id.lyt_webView);
        btnMobilePayment = (Button) findViewById(R.id.btn_subscribe);
        tvPaymentBangla = findViewById(R.id.tv_payment_bangla);
        tvPaymentEnglish = findViewById(R.id.tv_payment_english);
        //  btnCardPayment.setOnClickListener(this);
        btnMobilePayment.setOnClickListener(this);
        imgBtnBack = findViewById(R.id.img_btn_back);
        imgBtnBack.setOnClickListener(this);
        Glider.loadBannerImage(ivBanner, " ");


    }

    @Override
    protected void onResume() {
        super.onResume();
       /* Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {

            }
        });*/

        if (NetUtil.isNetConnected(this)) {
            internetChecking(true);
        } else {
            internetChecking(false);
        }

        //internetChecking(true);
        btnMobilePayment.setClickable(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SubscriptionActivity.this,
                MainActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.btn_card_payment:
                Intent cardPayment = new Intent(this, CardPaymentActivity.class);
                startActivity(cardPayment);
                break;*/

            case R.id.img_btn_back:
                startActivity(new Intent(SubscriptionActivity.this,
                        MainActivity.class));
                // onBackPressed();
                break;

            case R.id.btn_subscribe:

                logForFirebase();
                if (TextUtils.isEmpty(subApi)) {
                    Toast.makeText(this, "Something went wrong please try again"
                            , Toast.LENGTH_LONG).show();
                } else {
                    if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))
                            && !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {

                        if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDG111")) {
                            lytMobileBilling.setVisibility(View.GONE);
                            lytWebView.setVisibility(View.VISIBLE);
                            initGpWebView(subApi);

                        }

                        if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDB111")) {
                            if (getOperator().equalsIgnoreCase("Wifi")) {
                                if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDB111")) {
                                    Toast.makeText(SubscriptionActivity.this,
                                            "To Subscribe Use Banglalink Mobile Network",
                                            Toast.LENGTH_SHORT).show();
                                } else {

                                    lytMobileBilling.setVisibility(View.GONE);
                                    lytWebView.setVisibility(View.VISIBLE);
                                    initWebView(subApi);
                                }

                                Toast.makeText(SubscriptionActivity.this,
                                        getResources().getString(R.string.sub_bl_message_eng),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                lytMobileBilling.setVisibility(View.GONE);
                                lytWebView.setVisibility(View.VISIBLE);
                                initWebView(subApi);
                            }
                        } else {
                            progressBar.setVisibility(View.VISIBLE);
                            progressBar.setProgress(100);
                            btnMobilePayment.setClickable(false);
                            btnMobilePayment.setBackgroundColor(getResources().getColor(R.color.light_gray));
                            String baseUrl = subApi;
                            apiSubscription(baseUrl);
                        }
                    } else {
                        Toast.makeText(this, "Please Login First"
                                , Toast.LENGTH_LONG).show();
                    }
                }

                break;
        }

    }


    private void apiMessage() {
        if (!TextUtils.isEmpty(apiMessage)) {
            String baseUrl = apiMessage;
            apiMessage(baseUrl);
        }
    }

    private void apiBilling() {
        if (!TextUtils.isEmpty(apiBilling)) {
            String baseUrl = apiBilling;
            apiBilling(baseUrl);
        }
    }

    private void logForFirebase() {
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.ITEM_LIST, ++count);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SharedPref.read(Constant.USER_NAME));
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SharedPref.read(Constant.USER_PHONE_NO));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, bundle);
    }

    private void internetChecking(boolean isConnected) {
        if (isConnected) {
            if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))
                    && !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {

                String operatorName = SharedPref.read(Constant.USER_OPERATOR);
                if (operatorName.equalsIgnoreCase("BDB111") ||
                        operatorName.equalsIgnoreCase("BDG111")) {

                    if (operatorName.equalsIgnoreCase("BDB111")) {
                        checkBlDataConnection();
                    } else {
                        getBillingInfo(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));
 /*else {
                            tvPaymentBangla.setText("Please Try From Baglalink Mobile Data");
                            tvPaymentEnglish.setVisibility(View.GONE);
                            btnMobilePayment.setVisibility(View.GONE);

                        }*/
                    }

                } else {
                    // for robi
                    getBillingInfo(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));

                }
            }


        } else {
            Toast.makeText(SubscriptionActivity.this, "No Network Found",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void getBillingInfo(final String operator, String mobileNo) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<BillingItemResponse> call = jsonPostService.getBillingInfo(operator, mobileNo, "app");
        call.enqueue(new Callback<BillingItemResponse>() {

            @Override
            public void onResponse(@NonNull Call<BillingItemResponse> call, @NotNull Response<BillingItemResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        BillingItem billingItem = response.body().getItems();
                        if (billingItem == null)
                            return;
                        else {
                            if (!TextUtils.isEmpty(billingItem.getApiSub())) {

                                subApi = billingItem.getApiSub();
                                loadSubApi = billingItem.getLoadSubApi();
                                apiMessage = billingItem.getApiMessage();
                                apiBilling = billingItem.getApiBilling();
                                messageBody = billingItem.getMessageBody();
                                billingAmount = billingItem.getBillingAmount();
                                String messageBN = billingItem.getMessageBN();
                                String messageEN = billingItem.getMessageEN();
                                Glider.loadImage(ivOperator, billingItem.getLogo());
                                tvPaymentBangla.setText(messageBN);
                                tvPaymentEnglish.setText(messageEN);


                            }
                            String service = billingItem.getService();
                            String message = billingItem.getMessage();

                            if (!TextUtils.isEmpty(service)) {
                                if (service.equals("no")) {
                                    btnMobilePayment.setVisibility(View.GONE);
                                    tvPaymentBangla.setText(message);
                                    return;
                                }
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<BillingItemResponse> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private String getOperator() {
        String network = "";

        switch (NetUtil.getNetworkInfo(this)) {
            case "Mobile":
                network = "Mobile";
                break;
            case "Wifi":
                startActivity(new Intent(this,
                        RegistrationActivity.class));
                network = "Wifi";
                break;
            default:
                network = "Wifi";
                break;
        }
        return network;
    }

    @Override
    protected void onStop() {
        // Tovuti.from(this).stop();
        super.onStop();
    }

    private void apiSubscription(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String string = response.body();
                        if (string.equals("1")) {
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                            apiBilling();
                            apiMessage();
                            Toast.makeText(SubscriptionActivity.this, "Request Accepted", Toast.LENGTH_SHORT).show();
                            Intent intent = null;
                            if (TextUtils.isEmpty(getActivityTag()) && TextUtils.isEmpty(getDirectSub())) {
                                //intent = new Intent(SubscriptionActivity.this, PlayerActivity.class);
                                intent = new Intent(SubscriptionActivity.this, DetailActivity.class);
                                intent.putExtra(Constant.CONTENT_URL, getUrl());
                                intent.putExtra(Constant.CONTENT_TYPE, getContentType());
                                intent.putExtra(Constant.CONTENT_ID, getContentId());
                                intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, isPremium());
                                intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, getFreePlayTime());
                                intent.putExtra(Constant.CONTENT_NAME, getContentName());
                                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            } else {
                                intent = new Intent(SubscriptionActivity.this, MainActivity.class);

                            }
                            startActivity(intent);
                            finish();

                            //btnMobilePayment.setBackgroundColor(R.color.white);
                            // btnMobilePayment.setOnClickListener(null);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SubscriptionActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
                btnMobilePayment.setClickable(true);
                btnMobilePayment.setBackgroundColor(getResources().getColor(R.color.red));


            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                progressBar.setVisibility(View.GONE);
                // Toast.makeText(SubscriptionActivity.this, "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void apiMessage(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                    //Toast.makeText(SubscriptionActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
                // Toast.makeText(SubscriptionActivity.this, "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void apiBilling(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SubscriptionActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
            }
        });
    }

    private String getUrl() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_URL);
        return url;
    }

    private String isPremium() {
        String premium = "";
        Intent intent = getIntent();
        if (intent != null)
            premium = intent.getStringExtra(Constant.CONTENT_PREMIUM_STATUS);
        return premium;
    }


    private String getContentName() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_NAME);
        return url;
    }

    private String getContentId() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_ID);
        return url;
    }

    private String getContentType() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_TYPE);
        return url;
    }

    private String getFreePlayTime() {
        String time = "";
        Intent intent = getIntent();
        if (intent != null)
            time = intent.getStringExtra(Constant.CONTENT_FREE_PLAY_TIME);
        return time;
    }

}
