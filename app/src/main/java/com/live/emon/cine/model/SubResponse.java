package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("messageBody")
    @Expose
    private String messageBody;

    public String getStatus() {
        return status;
    }

    public void setStatus(String op) {
        this.status = op;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String mobileNo) {
        this.messageBody = mobileNo;
    }
}
