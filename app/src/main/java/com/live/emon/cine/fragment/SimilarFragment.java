package com.live.emon.cine.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.live.emon.cine.R;
import com.live.emon.cine.adapter.HorizontalRecyclerViewAdapter;
import com.live.emon.cine.adapter.MoviesAdapter;
import com.live.emon.cine.helper.MyDividerItemDecoration;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SimilarFragment extends Fragment {

    private List<Item> movieList;
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private View view;
    private HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;
    private List<Item> items;
    private Call<ItemResponse> responseCall;
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private String mParam;



    public SimilarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SimilarFragment newInstance(String mParam) {
        SimilarFragment fragment = new SimilarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, mParam);
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_similar, container, false);
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM1);
            if (!TextUtils.isEmpty(mParam))
                searchContent(mParam.trim());
        }
    }

    private void initView() {
        movieList = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.recy_view_more_like);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(2,
                Objects.requireNonNull(getActivity()).getResources().getColor(R.color.border_color)));

        mAdapter = new MoviesAdapter(getActivity(), movieList);

        recyclerView.setAdapter(mAdapter);
        recyclerView.setFocusable(false);
        recyclerView.setHasFixedSize(true);
     }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void searchContent(String searchKey) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        responseCall = apiService.searchContent(searchKey);
        responseCall.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() == null)
                        return;
                     movieList = response.body().getItems();
                    Timber.e(movieList.toString());
                    mAdapter = new MoviesAdapter(getActivity(), movieList);
                    recyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ItemResponse> call, @NonNull Throwable t) {

            }
        });
    }

}
