package com.live.emon.cine.activity;



import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.live.emon.cine.R;
import com.live.emon.cine.fragment.ComingSoonFragment;
import com.live.emon.cine.fragment.HomeFragment;
import com.live.emon.cine.fragment.MoreFragment;
import com.live.emon.cine.fragment.SearchFragment;
import com.live.emon.cine.helper.BottomNavigationViewHelper;

import com.live.emon.cine.utils.ForceUpdateChecker;



public class MainActivity extends AppCompatActivity  implements ForceUpdateChecker.OnUpdateNeededListener{

    private Fragment fragment;
    private FragmentManager fragmentManager;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //  initToolbar();
       // forceCrash(bottomNavigation);
        initView();
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }



    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
          super.onPause();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initToolbar() {
        Toolbar mToolbar =  findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        setSupportActionBar(mToolbar);

        TextView tvToolbarFont =  findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setText(getString(R.string.app_name));
        tvToolbarFont.setTypeface(font);
    }

    private void initView() {
        BottomNavigationView bottomNavigation =  findViewById(R.id.navigationView);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
        BottomNavigationViewHelper.changeBottomNavIcon(this, bottomNavigation);
        // attaching bottom sheet behaviour - hide / show on scroll

        //CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigation.getLayoutParams();
        //  layoutParams.setBehavior(new BottomNavigationBehavior());

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, new HomeFragment()).commit();

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.navigation_home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.navigation_search:
                        fragment = new SearchFragment();
                        break;
                   /* case R.id.navigation_comming_soon:
                        fragment = new CinematicOfflineFragment();
                        break;
*/
                    case R.id.navigation_trailer:
                        fragment = new ComingSoonFragment();
                        break;

                    case R.id.navigation_more:
                        fragment = new MoreFragment();
                        break;
                }
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container, fragment).commit();
                transaction.addToBackStack(null);
                return true;
            }
        });
    }


    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
