package com.live.emon.cine.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;


import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.BuildConfig;
import com.live.emon.cine.R;

import com.live.emon.cine.activity.MainActivity;
import com.live.emon.cine.adapter.SlidingImageAdapter;
import com.live.emon.cine.adapter.VerticalRecyclerViewAdapter;

import com.live.emon.cine.model.*;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;

import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import timber.log.Timber;

public class HomeFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    // SliderLayout sliderLayout;
    private View view;

    private VerticalRecyclerViewAdapter mAdapter;

    //private SwipeRefreshLayout swipeRefresh;
    // private BottomSheetBehavior sheetBehavior;
    private List<Category> categories;
    private Call<ItemResponse> callTopContent;
    private Call<CategoryResponse> callCategory;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;
    //  private Tovuti tovuti;
    private int jumpPosition = -1;
    private ProgressBar progressBar;

    public HomeFragment() {
    }

    public void onBackPressed() {

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        initToolbar();
        initView();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (NetUtil.isNetConnected(getActivity())) {
            loadData(true);
        } else {
            loadData(false);
        }

        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    exit();
                    return true;
                }
                return false;
            }
        });

    }

    private void exit() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }


    private void loadData(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            loadTopContent();
            loadCategory();

        } else {
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);

        }
    }


    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");

        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(mToolbar);


        TextView tvToolbarFont = (TextView) view.findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }
        tvToolbarFont.setTypeface(font);
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        //sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (callTopContent != null) {
            callTopContent.cancel();
            callCategory.cancel();
        }
    }


    private void initView() {

        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

  /*      swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        int myColor = Color.parseColor("#494848");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.BLACK, Color.GRAY);
        swipeRefresh.setRefreshing(true);
*/
        /*LinearLayout layoutBottomSheet = (LinearLayout) view.findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
*/
        relativeLytNoNetwork = (RelativeLayout) view.findViewById(R.id.relative_lyt_no_network);
        imgNoNetLogo = (ImageView) view.findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = (TextView) view.findViewById(R.id.tv_something_wrong);
        tvfailedMessage = (TextView) view.findViewById(R.id.failed_message);


        btnFailedRetry = (Button) view.findViewById(R.id.btn_failed_retry);
        btnFailedRetry.setOnClickListener(this);
       /* tvItemMessage = (TextView) findViewById(R.id.no_item_message);
        tvItemMessage.setOnClickListener(this);
       */
        btnFailedRetry.setOnClickListener(this);


        categories = new ArrayList<>();

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
       /* sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {

                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {

                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });*/
        //delay for testing empty view functionality
      /*  slider.postDelayed(new Runnable() {
            @Override
            public void run() {
                slider.setAdapter(new MainSliderAdapter(getActivity()));
                slider.setSelectedSlide(0);
            }
        }, 1500);*/

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new VerticalRecyclerViewAdapter(getActivity(), categories);
        recyclerView.setAdapter(mAdapter);
        recyclerView.smoothScrollToPosition(0);

    }


    private void loadContent(final List<Category> categories) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        final List<Category> updateList = new ArrayList<>();

        for (int i = 0; i < categories.size(); i++) {
            final Category category = categories.get(i);
            if (category.getCategory().equalsIgnoreCase("continue watching")) {
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                    getContinueWatchMovies(SharedPref.read(Constant.USER_PHONE_NO), category, updateList);
                }
            }

            if (category.getCategory().equalsIgnoreCase("my list")) {
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                    loadMyWatchList(SharedPref.read(Constant.USER_PHONE_NO), category, updateList);
                }
            }
            Call<ItemResponse> call = apiService.getItemsByCategory(category.getCategory());
            call.enqueue(new Callback<ItemResponse>() {
                @Override
                public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        List<Item> movies = response.body().getItems();
                        if (movies != null && movies.size() != 0) {
                            category.setArrayList(movies);
                            if (!updateList.contains(category)) {
                                updateList.add(category);
                                // mAdapter.setList(categories);
                            }
                        }
                        if (!mAdapter.getmArrayList().contains(updateList) && movies.size() != 0) {
                            mAdapter.clear();
                            if (category.getArrayList() != null)
                                mAdapter.setList(updateList);
                        }
                        mAdapter.notifyDataSetChanged();
                        //swipeRefresh.setRefreshing(false);
                        Timber.v("Movies %s", movies.toString());
                    }


                    //Log.d(TAG, "Number of Item Recived: " + movies.size());
                }

                @Override
                public void onFailure(Call<ItemResponse> call, Throwable t) {
                    // Log error here since request failed
                    // Timber.e(t.toString());
                }
            });
        }
    }


    private void loadCategory() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        callCategory = apiService.getCategories();
        callCategory.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<CategoryResponse> call, @NonNull Response<CategoryResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() == null)
                        return;
                    categories = response.body().getCategories();
                    if (categories.size() > 0) {
                        loadContent(categories);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryResponse> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }


    private void loadTopContent() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        callTopContent = apiService.getTopContent();
        callTopContent.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NotNull Response<ItemResponse> response) {

                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    // swipeRefresh.setRefreshing(false);
                    if (response.body() == null) return;
                    List<Item> items = response.body().getItems();
                    init(items);
                }
            }


            @Override
            public void onFailure(@NonNull Call<ItemResponse> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });

    }


    private void getContinueWatchMovies(String userId, final Category category, final List<Category> updateList) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        final List<Item> movies = new ArrayList<>();
        Call<ContinueWatchItemResponse> callCategory = apiService.getContinueWatchMovies(userId);
        callCategory.enqueue(new Callback<ContinueWatchItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ContinueWatchItemResponse> call, @NonNull Response<ContinueWatchItemResponse> response) {

                if (response.isSuccessful()) {
                    ContinueWatchItemResponse continueWatchItemResponse = response.body();
                    if (continueWatchItemResponse.getCount() > 0) {
                        List<ContinueWatchItem> continueWatchItems = continueWatchItemResponse.getItems();
                        for (int i = 0; i < continueWatchItems.size(); i++) {
                            ContinueWatchItem continueWatchItem = continueWatchItems.get(i);
                            Item item = new Item();
                            item.setPremium(continueWatchItem.getPremium());
                            item.setLength(continueWatchItem.getContentLength());
                            item.setPlayPercentDuration(continueWatchItem.getPlayPercentDuration());
                            item.setPlayTime(continueWatchItem.getPlayTime());
                            item.setThumbnail(continueWatchItem.getThumbnail());
                            item.setContentId(continueWatchItem.getContentId());
                            item.setUserid(continueWatchItem.getUserid());
                            item.setCategory("continue watching");
                            movies.add(item);
                            if (movies != null && movies.size() != 0) {
                                category.setArrayList(movies);
                                if (!updateList.contains(category)) {
                                    updateList.add(category);
                                    // mAdapter.setList(categories);
                                }
                            }
                            if (!mAdapter.getmArrayList().contains(updateList) && movies.size() != 0) {
                                mAdapter.clear();
                                if (category.getArrayList() != null)
                                    mAdapter.setList(updateList);
                            }
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ContinueWatchItemResponse> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }


    private void loadMyWatchList(String userId, final Category category, final List<Category> updateList) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<WatchListResponseResponse> call = apiService.getMyWatchList(userId);
        call.enqueue(new Callback<WatchListResponseResponse>() {
            @Override
            public void onResponse(@NonNull Call<WatchListResponseResponse> call, Response<WatchListResponseResponse> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    List<WatchItem> itemList = response.body().getItems();
                    if (itemList != null) {

                        List<Item> movies = new ArrayList<>();
                        for (int i = 0; i < itemList.size(); i++) {
                            if (itemList.get(i).getWatchStatus().equals("1")) {
                                // watchListAdapter.clear();
                                Item item = new Item();
                                item.setPremium(itemList.get(i).getPremium());
                                item.setContentId(itemList.get(i).getContentId());
                                item.setThumbnail(itemList.get(i).getThumbnail());
                                item.setCategory("my list");
                                if (!movies.contains(item)) {
                                    movies.add(item);
                                }
                            }
                        }

                        category.setArrayList(movies);
                        if (!updateList.contains(category)) {
                            updateList.add(category);
                            // mAdapter.setList(categories);
                        }

                        if (!mAdapter.getmArrayList().contains(updateList) && movies.size() != 0) {
                            mAdapter.clear();
                            if (category.getArrayList() != null)
                                mAdapter.setList(updateList);
                        }
                        mAdapter.notifyDataSetChanged();
                        //swipeRefresh.setRefreshing(false);
                        Timber.v("Movies %s", movies.toString());


                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<WatchListResponseResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onRefresh() {
        Tovuti.from(getActivity()).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });
    }

    private void init(List<Item> items) {


        mPager = view.findViewById(R.id.pager);

        if (getActivity() != null) {
            mPager.setAdapter(new SlidingImageAdapter(getActivity(), items));
        }


       /* CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);*/


        NUM_PAGES = items.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 20 * 1000, 50 * 1000);

        // Pager listener over indicator
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                //swipeRefresh.setRefreshing(false);
                currentPage = position;

                mPager.setCurrentItem(currentPage, true);

                /*if (position == 0) {
                    //prepare to jump to the last page
                    jumpPosition = NUM_PAGES;

                    //TODO: indicator.setActive(adapter.getRealCount() - 1)
                } else if (position == NUM_PAGES + 1) {
                    //prepare to jump to the first page
                    jumpPosition = 1;

                    //TODO: indicator.setActive(0)
                } else {
                    //TODO: indicator.setActive(position - 1)

                }*/

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {
               /* if (pos == ViewPager.SCROLL_STATE_IDLE && jumpPosition >= 0) {
                    //Jump without animation so the user is not aware what happened.
                    mPager.setCurrentItem(jumpPosition, false);
                    //Reset jump position.
                    jumpPosition = -1;
                }*/
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_failed_retry:
                if (NetUtil.isNetConnected(getActivity())) {
                    loadData(true);
                } else {
                    loadData(false);
                }
            default:
                break;
        }

    }


}
