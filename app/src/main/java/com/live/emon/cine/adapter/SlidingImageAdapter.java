package com.live.emon.cine.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.images.internal.ImageUtils;
import com.live.emon.cine.R;
import com.live.emon.cine.activity.DetailActivity;
import com.live.emon.cine.activity.PlayerActivity;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.CaseConverter;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.DrawableUtil;
import com.live.emon.cine.utils.Glider;
import com.live.emon.cine.utils.ImageUtil;
import com.live.emon.cine.utils.SharedPref;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class SlidingImageAdapter extends PagerAdapter implements View.OnClickListener {
    private ImageButton ivMyList;
    private List<Item> items;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImageAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert imageLayout != null;

        final FrameLayout lytMain = (FrameLayout) imageLayout
                .findViewById(R.id.lyt_main);
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);
        final TextView tVSubCategory = (TextView) imageLayout
                .findViewById(R.id.tv_sub_category);


        final TextView tvTitle = (TextView) imageLayout
                .findViewById(R.id.tv_title);

       /* final TextView tvRoyality = (TextView) imageLayout
                .findViewById(R.id.tv_royality);
*/
        final TextView tvYear = (TextView) imageLayout
                .findViewById(R.id.tv_year);

       /* ivMyList = (ImageButton) imageLayout
                .findViewById(R.id.iv_my_list);
*/
        /*final ImageView ivDetail = (ImageView) imageLayout
                .findViewById(R.id.iv_detail);
        */final Button btnPlay = (Button) imageLayout
                .findViewById(R.id.btn_play);

        imageView.requestFocus();

        //ivDetail.setOnClickListener(this);
       // ivMyList.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        lytMain.setOnClickListener(this);

        Item item = items.get(position);


        if (item != null) {
            Glider.loadTopContentImage(imageView, item.getPortrait());
            tvTitle.setText(CaseConverter.camelCase(item.getAlbum()));
            tVSubCategory.setText(CaseConverter.camelCase(item.getSubcategory()));
           // tvRoyality.setText(CaseConverter.camelCase(item.getRoyality()));
            tvYear.setText(item.getYear());
            /*if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                getWatchState(SharedPref.read(Constant.USER_PHONE_NO), item.getContentId(), item.getPortrait(), ivMyList, "");
            }*/

            btnPlay.setTag(item);
            //ivMyList.setTag(item);
           // ivDetail.setTag(item);
            lytMain.setTag(item);
        }
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public void onClick(View v) {

        Item item = (Item) v.getTag();
        if (item == null)
            return;
        Intent intent;

        switch (v.getId()) {
            case R.id.btn_play:
                intent = new Intent(context, DetailActivity.class);
                intent.putExtra(Constant.CONTENT_ID, item.getContentId());
                context.startActivity(intent);
                break;

            case R.id.iv_my_list:
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                    /*if (DrawableUtil.areDrawablesIdentical(ivMyList.getDrawable(), context.getResources().getDrawable(R.drawable.ic_tick))) {
                        // Glide.with(context).load(ImageUtil.getImage(context,"ic_plus")).into(ivMyList);
                        ivMyList.setImageResource(R.drawable.ic_plus_symbol);
                        sendWatchStatus(SharedPref.read(Constant.USER_PHONE_NO), item.getContentId(), item.getPortrait(), "0");
                    } else {
                        ivMyList.setImageResource(R.drawable.ic_tick);
                        //  Glide.with(context).load(ImageUtil.getImage(context,"ic_tick")).into(ivMyList);
                        sendWatchStatus(SharedPref.read(Constant.USER_PHONE_NO), item.getContentId(), item.getPortrait(), "1");
                    }*/

                   // getWatchState(SharedPref.read(Constant.USER_PHONE_NO), item.getContentId(), item.getPortrait(), ivMyList, "0");

                } else {
                    Toast.makeText(context, "Please login frist to add watch list",
                            Toast.LENGTH_SHORT).show();
                }


                break;

           /* case R.id.iv_detail:
                intent = new Intent(context, DetailActivity.class);
                intent.putExtra(Constant.CONTENT_ID, item.getContentId());
                context.startActivity(intent);
                break;*/

            case R.id.lyt_main:
                intent = new Intent(context, DetailActivity.class);
                intent.putExtra(Constant.CONTENT_ID, item.getContentId());
                context.startActivity(intent);
                break;
        }


    }

    /*private void sendWatchStatus(String userId,
                                 String contentId,
                                 String portrait,
                                 final String watchState) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendWatchList(userId,
                contentId, portrait, watchState);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private void getWatchState(final String userId,
                               final String contentId,
                               final String portrait,
                               final ImageButton imageButton,
                               final String state) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<String> call = apiService.getWatchState(userId, contentId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    String watchState = response.body();
                    if (!TextUtils.isEmpty(watchState)) {
                        if (watchState.equals("1")) {
                            imageButton.setImageResource(R.drawable.ic_tick);
                            if (!TextUtils.isEmpty(state)) {
                                imageButton.setImageResource(R.drawable.ic_plus_symbol);
                                sendWatchStatus(userId, contentId, portrait, "0");
                            }
                            //imageButton.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_tick));
                        } else {
                            // imageButton.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_plus));
                            imageButton.setImageResource(R.drawable.ic_plus_symbol);
                            if (!TextUtils.isEmpty(state)) {
                                imageButton.setImageResource(R.drawable.ic_tick);
                                sendWatchStatus(userId, contentId, portrait, "1");
                            }

                        }
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }*/

}
