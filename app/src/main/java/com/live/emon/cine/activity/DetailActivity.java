package com.live.emon.cine.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Pair;
import android.view.*;
import android.widget.*;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.google.android.exoplayer2.*;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.*;
import com.google.android.exoplayer2.ui.*;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.ViewPagerAdapter;
import com.live.emon.cine.app.CinematicApp;
import com.live.emon.cine.database.DatabaseHelper;
import com.live.emon.cine.database.TimeTracker;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.model.OperatorInfo;
import com.live.emon.cine.model.SubResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.*;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DetailActivity extends AppCompatActivity
        implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener,
        Player.EventListener,
        PlayerControlView.VisibilityListener, PlaybackPreparer {

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ImageView ivItemLand, ivMyList, ivShare, ivRate;
    private TextView tvName, tvYear, tvTime, tvDescription, tvStarring,
            tvDirecting;
    private Item item;
    private SwipeRefreshLayout swipeRefresh;
    private RelativeLayout relativeLytNoNetwork;
    private LinearLayout lytContent;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;
    private Dialog dialog;
    private boolean isPlaying;
    // player variable
    public static final String PREFER_EXTENSION_DECODERS_EXTRA = "prefer_extension_decoders";

    private DatabaseHelper db;
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private ProgressBar progressBar;
    private boolean playWhenReady;
    private int currentWindow;
    private long playbackPosition;
    private FrameworkMediaDrm mediaDrm;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private FirebaseAnalytics mFirebaseAnalytics;
    //private boolean playerState;
    private int timeCount = 0;
    private CountDownTimer waitTimer;

    private TrackGroupArray lastSeenTrackGroupArray = null;
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";

    private boolean mExoPlayerFullscreen = false;
    private int mResumeWindow;
    private long mResumePosition;
    private final String STATE_RESUME_WINDOW = "resumeWindow";
    private final String STATE_RESUME_POSITION = "resumePosition";
    private final String STATE_PLAYER_FULLSCREEN = "playerFullscreen";
    private final String PLAYER_URL = "playerFullscreen";
    private Dialog mFullScreenDialog;
    private ImageView mFullScreenIcon;
    private FrameLayout mFullScreenButton;
    private LinearLayout lyt_main;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        initFirebaseAndDB();
        initToolbar();
        initExoPlayerView();

        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW);
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN);
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);

            //startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            // startWindow = savedInstanceState.getInt(KEY_WINDOW);
            // startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().build();
            // clearStartPosition();
        }

        initView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow);
        outState.putLong(STATE_RESUME_POSITION, mResumePosition);
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen);
        super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    closeFullscreenDialog();
                }

                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog() {
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        mFullScreenDialog.addContentView(playerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_skrink));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {

        ((ViewGroup) playerView.getParent()).removeView(playerView);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(playerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_expand));
    }


    private void initFullscreenButton() {

        PlaybackControlView controlView = playerView.findViewById(R.id.exo_controller);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    openFullscreenDialog();
                } else {
                    closeFullscreenDialog();
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
    }


    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }


    private void logForFireBase() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_LIST, item.getTitle());
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SharedPref.read(Constant.USER_PHONE_NO));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    private void initExoPlayerView() {

        ImageView ivPlayButton = findViewById(R.id.exo_play);
        ImageView ivPauseButton = findViewById(R.id.exo_pause);
        ImageView ivFastForwardButton = findViewById(R.id.exo_ffwd);
        ImageView ivReWindButton = findViewById(R.id.exo_rew);
        ImageView ivSetting = findViewById(R.id.exo_setting);
        ivBack = findViewById(R.id.iv_back);
        lyt_main = findViewById(R.id.lyt_main);

        ivPlayButton.setOnClickListener(this);
        ivPauseButton.setOnClickListener(this);
        ivFastForwardButton.setOnClickListener(this);
        ivReWindButton.setOnClickListener(this);
        ivSetting.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        // lytBack.setOnClickListener(this);

        ivBack.setVisibility(View.GONE);

        progressBar = findViewById(R.id.progress_bar);

        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        playerView = findViewById(R.id.player_view);

        playerView.setControllerVisibilityListener(this);
        playerView.requestFocus();
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);


        initFullscreenDialog();
        initFullscreenButton();


        if (mExoPlayerFullscreen) {
            ((ViewGroup) playerView.getParent()).removeView(playerView);
            mFullScreenDialog.addContentView(playerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_skrink));
            mFullScreenDialog.show();
        }

    }


    private void initFirebaseAndDB() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        db = new DatabaseHelper(this);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        if (!TextUtils.isEmpty(getContentId())) {

            List<TimeTracker> timeTrackers = db.getAllNotes();

            if (timeTrackers.size() != 0) {
                for (TimeTracker timeTracker : timeTrackers) {
                    // find db contentID
                    String contentId = timeTracker.getContentId();
                    if (!contentId.equals(getContentId())) {
                        db.insertTime(new TimeTracker(getContentId(), 0));
                        break;
                    }
                }
            } else {
                db.insertTime(new TimeTracker(getContentId(), 0));
            }
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            //  if (playerState)
            initializePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            //  if (playerState)
            initializePlayer();
        }

        if (NetUtil.isNetConnected(this)) {
            loadData(true);
        } else {
            loadData(false);
        }

       /* //loadData(isConnected);
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });*/


        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
            getWatchState(SharedPref.read(Constant.USER_PHONE_NO), getContentId());
        }

        // when content play send log to firebase


    }

    private void checkingUserContentStatus(final Item item) {

        // int saveTimePre = SharedPref.readInt(getContentId());
        TimeTracker timeTracker = db.getTime(getContentId());
        int saveTime = 0;
        if (timeTracker != null) {
            saveTime = timeTracker.getTime();
        }
        int freePlayTime = 0;
        if (!TextUtils.isEmpty(item.getFreePlayTime())) {
            freePlayTime = Integer.valueOf(item.getFreePlayTime());
        }
        /*TODO
         * free play time checking */
        int remainingTime = freePlayTime - saveTime;
        // int timeInSec = remainingTime - saveTime;
        if (remainingTime > 0) {
            waitTimer = new CountDownTimer(remainingTime * 1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    timeCount++;
                    //called every 300 milliseconds, which could be used to
                    //send messages or some other action
                }

                public void onFinish() {

                    int freePlayTime = 0;
                    if (!TextUtils.isEmpty(item.getFreePlayTime())) {
                        freePlayTime = Integer.valueOf(item.getFreePlayTime());
                    }

                    timeCount = freePlayTime;
                    // SharedPref.writeInt(getContentId(), timeCount);
                    TimeTracker timeTracker = new TimeTracker(getContentId(), timeCount);
                    db.updateNote(timeTracker);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // registered user

                            if (!TextUtils.isEmpty(item.getPremium()) && item.getPremium().equals(Constant.CONTENT_PREMIUM)) {
                                // content premium
                                String mobileNo = SharedPref.read(Constant.USER_PHONE_NO);
                                String operator = SharedPref.read(Constant.USER_OPERATOR);
                                if (!TextUtils.isEmpty(mobileNo) && !TextUtils.isEmpty(operator)
                                        && mobileNo.length() > 12 && operator.length() > 5) {
                                    if (SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS)
                                            .equalsIgnoreCase("1")) {
                                        if (player != null)
                                            playbackPosition = player.getCurrentPosition();
                                        SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                                        if (!TextUtils.isEmpty(item.getFile()))
                                            playContent(item.getFile(), true);
                                    } else {
                                        // already have mobile no and operator so go to sub page
                                        if (player != null)
                                            player.stop();
                                        if (item != null) {
                                            String operatorName = SharedPref.read(Constant.USER_OPERATOR);
                                            if (!TextUtils.isEmpty(operatorName) && operatorName.equalsIgnoreCase("BDB111")) {

                                                checkBlDataConnection();

                                            } else {
                                                openCustomDialog("", item);
                                            }

                                        }
                                    }
                                } else {
                                    // check wifi and mobile
                                    if (player != null) {
                                        player.stop();
                                    }
                                    getOperator();
                                }

                            } else {
                                // free content
                                if (player != null)
                                    playbackPosition = player.getCurrentPosition();
                                SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                                if (!TextUtils.isEmpty(item.getFile()))
                                    playContent(item.getFile(), true);

                            }
                        }
                    });

                }
            }.start();

        } else {
            int freePTime = 0;
            if (!TextUtils.isEmpty(item.getFreePlayTime())) {
                freePTime = Integer.valueOf(item.getFreePlayTime());
            }
            timeCount = freePTime;
            // SharedPref.writeInt(getContentId(), timeCount);
            timeTracker = new TimeTracker(getContentId(), timeCount);
            db.updateNote(timeTracker);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // registered user
                    if (!TextUtils.isEmpty(item.getPremium()) && item.getPremium().equals(Constant.CONTENT_PREMIUM)) {
                        // content premium
                        String mobileNo = SharedPref.read(Constant.USER_PHONE_NO);
                        String operator = SharedPref.read(Constant.USER_OPERATOR);
                        if (!TextUtils.isEmpty(mobileNo) && !TextUtils.isEmpty(operator)
                                && mobileNo.length() > 12 && operator.length() > 5) {
                            if (SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS)
                                    .equalsIgnoreCase("1")) {
                                if (player != null)
                                    playbackPosition = player.getCurrentPosition();
                                SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                                if (!TextUtils.isEmpty(item.getFile()))
                                    playContent(item.getFile(), true);

                            } else {
                                // already have mobile no and operator so go to sub page
                                if (player != null)
                                    player.stop();
                                if (item != null) {
                                    String operatorName = SharedPref.read(Constant.USER_OPERATOR);
                                    if (!TextUtils.isEmpty(operatorName) && operatorName.equalsIgnoreCase("BDB111")) {
                                        checkBlDataConnection();
                                    } else {
                                        openCustomDialog("", item);
                                    }

                                }

                            }
                        } else {
                            // check wifi and mobile
                            if (player != null)
                                player.stop();
                            getOperator();
                        }
                    } else {
                        // free content
                        // SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                        if (!TextUtils.isEmpty(item.getFile()))
                            playContent(item.getFile(), true);

                    }
                }
            });

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
        if (mFullScreenDialog != null)
            mFullScreenDialog.dismiss();

        //  Tovuti.from(this).stop();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
           /* View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);*/
            hideSoftKey();
            ivBack.setVisibility(View.VISIBLE);

        } else {
            ivBack.setVisibility(View.GONE);

        }
    }

    public void hideSoftKey() {
        //for new api versions.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

   /* @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }*/

    private void loadData(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(getContentId()))
                loadContent(getContentId());
        } else {
            swipeRefresh.setRefreshing(false);
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }


    private void getWatchState(String userId, String contentId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<String> call = apiService.getWatchState(userId, contentId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    String watchState = response.body();
                    if (!TextUtils.isEmpty(watchState)) {
                        if (watchState.equals("1")) {
                            ivMyList.setImageDrawable(getResources().getDrawable(R.drawable.ic_tick));
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
            }
        });
    }

    private void loadContent(String contentId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ItemResponse> call = apiService.getItem(contentId);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getCount() != 0) {
                        List<Item> items = response.body().getItems();
                        if (items.size() > 0)
                            item = items.get(0);
                        showDetails(item);
                        if (!TextUtils.isEmpty(item.getArtist()) || !TextUtils.isEmpty(item.getTitle())) {
                            String artistName = getArtistName(item.getArtist());
                            String albamName = "";
                            if (!TextUtils.isEmpty(item.getAlbum())) {
                                albamName = item.getAlbum();
                            }
                            adapter = new ViewPagerAdapter(DetailActivity.this, getSupportFragmentManager(), artistName.toLowerCase(), albamName);
                            // Set the adapter onto the view pager
                            viewPager.setAdapter(adapter);
                        }
                        swipeRefresh.setRefreshing(false);
                    } else {
                        Toast.makeText(DetailActivity.this,
                                "No Item Found", Toast.LENGTH_SHORT).show();
                        swipeRefresh.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ItemResponse> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });


    }


    private void showDetails(Item item) {

        if (item != null) {
            tvName.setText(item.getTitle());
            tvYear.setText(item.getYear());
            tvTime.setText(item.getLength());
            ///tvDescription.setText(item.getDescription());
            String staring = String.format(
                    " " + item.getArtist());
            tvStarring.setText(staring);
            String director = String.format(
                    " " + item.getDirector());
            tvDirecting.setText(director);
            // Glider.showWithPlaceholder(ivItemLand, item.getLandscape());
            if (!TextUtils.isEmpty(getWatchState())) {
                if (getWatchState().equals("1")) {
                    ivMyList.setImageDrawable(getResources().getDrawable(R.drawable.ic_tick));
                }

            }
            playContent(item.getFile(), true);
            checkingUserContentStatus(item);
            logForFireBase();
            if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                sendUserActivity(SharedPref.read(Constant.USER_PHONE_NO), getContentId(), item.getType(), item.getTitle());
            }

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (item != null) {
            if (!TextUtils.isEmpty(item.getFreePlayTime())) {
                releaseAllWhenBackPress(item.getFreePlayTime());
            }
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                menuItem.setIcon(R.drawable.ic_left_arrow);

                if (item != null) {
                    if (!TextUtils.isEmpty(item.getFreePlayTime())) {
                        releaseAllWhenBackPress(item.getFreePlayTime());
                    }
                }

                return true;

            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }


    private String getContentId() {
        String contentId = "";
        Intent intent = getIntent();
        if (intent != null) {
            contentId = intent.getStringExtra(Constant.CONTENT_ID);
        }
        return contentId;
    }

    private String getCategory() {
        String category = "";
        Intent intent = getIntent();
        if (intent != null) {
            category = intent.getStringExtra(Constant.CONTENT_CATEGORY);
        }
        return category;
    }

    private String getWatchState() {
        String watchState = "";
        Intent intent = getIntent();
        if (intent != null) {
            watchState = intent.getStringExtra(Constant.CONTENT_WATCH_STATE);
        }
        return watchState;
    }


    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

       /* TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);
        tvToolbarFont.setText(getString(R.string.app_name));*/
    }


    private void initView() {


        relativeLytNoNetwork = findViewById(R.id.relative_lyt_no_network);
        //lytContent = findViewById(R.id.lyt_content);

        imgNoNetLogo = findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = findViewById(R.id.tv_something_wrong);
        tvfailedMessage = findViewById(R.id.failed_message);

        btnFailedRetry = findViewById(R.id.btn_failed_retry);
        btnFailedRetry.setOnClickListener(this);


        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        int myColor = Color.parseColor("#494848");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.BLACK, Color.GRAY);
        swipeRefresh.setRefreshing(true);

        tvName = findViewById(R.id.tvName);
        tvYear = findViewById(R.id.tvYear);
        tvTime = findViewById(R.id.tvTime);
        tvDescription = findViewById(R.id.tvDescription);
        tvStarring = findViewById(R.id.tvStarring);
        tvDirecting = findViewById(R.id.tvDirecting);
        //ivItemLand = findViewById(R.id.player_view);
        ivMyList = findViewById(R.id.iv_my_list);
        ivShare = findViewById(R.id.iv_my_share);
        ivRate = findViewById(R.id.iv_rate);
        ivMyList.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivRate.setOnClickListener(this);

        TabLayout tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewpager);

        if (TextUtils.isEmpty(SharedPref.read(getContentId() + Constant.LIKED))) {
            ivRate.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumbs_up));
        } else {
            ivRate.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumbs_up_blue));
        }

        tabLayout.setupWithViewPager(viewPager);

        ImageView ivPlayButton = findViewById(R.id.image_button_play);
        ivPlayButton.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.image_button_play:
                goToPlayerActivity(item);
                break;

            case R.id.iv_rate:
                if (DrawableUtil.areDrawablesIdentical(ivRate.getDrawable(), getResources().getDrawable(R.drawable.ic_thumbs_up))) {
                    SharedPref.write(getContentId() + Constant.LIKED, Constant.LIKED);
                    ivRate.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumbs_up_blue));
                } else {
                    ivRate.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumbs_up));
                    SharedPref.write(getContentId() + Constant.LIKED, "");

                    //  SharedPref.write(getContentId() + Constant.NOT_LIKED, Constant.NOT_LIKED);

                }
                break;

            case R.id.iv_my_share:
                shareAppLink();
                break;

            case R.id.iv_my_list:
                if (TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                    Toast.makeText(DetailActivity.this, "Please Subscribe First", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (DrawableUtil.areDrawablesIdentical(ivMyList.getDrawable(), getResources().getDrawable(R.drawable.ic_tick))) {

                    if (item != null) {
                        sendWatchStatus(SharedPref.read(Constant.USER_PHONE_NO), item.getContentId(), item.getPortrait(), "0", item.getPremium());
                    }
                    ivMyList.setImageDrawable(getResources().getDrawable(R.drawable.ic_plus));

                } else {
                    if (item != null) {
                        sendWatchStatus(SharedPref.read(Constant.USER_PHONE_NO), item.getContentId(), item.getPortrait(), "1", item.getPremium());
                    }
                    ivMyList.setImageDrawable(getResources().getDrawable(R.drawable.ic_tick));
                }
                break;

            case R.id.btn_failed_retry:
              /*  Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
                    @Override
                    public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                        loadData(isConnected);
                    }
                });*/

                if (NetUtil.isNetConnected(this)) {
                    loadData(true);
                } else {
                    loadData(false);
                }
                break;

            case R.id.exo_play:
                player.setPlayWhenReady(true);
                playbackPosition = player.getPlaybackState();
                if (player.getPlaybackState() == Player.STATE_ENDED)
                    player.seekTo(0);

                break;
            case R.id.exo_pause:
                if (player != null) {
                    player.setPlayWhenReady(false);
                    player.getPlaybackState();
                }
                break;
            case R.id.exo_ffwd:
                fastForward();
                break;
            case R.id.exo_rew:
                fastRewind();
                break;

            case R.id.exo_setting:
                trackerSelector(v);
                break;

            case R.id.iv_back:

                if (!mExoPlayerFullscreen) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    // lyt_main.setVisibility(View.GONE);
                    openFullscreenDialog();
                } else {
                    closeFullscreenDialog();
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                break;
            default:
                break;

        }

    }


    private void sendWatchStatus(String userId,
                                 String contentId,
                                 String portrait,
                                 String watchState,
                                 String premium) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendWatchList(userId,
                contentId, portrait, watchState, premium);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private void goToPlayerActivity(Item item) {

        if (item != null) {

            Intent intent = new Intent(this, PlayerActivity.class);
            intent.putExtra(Constant.CONTENT_URL, item.getFile());
            intent.putExtra(Constant.CONTENT_CATEGORY, item.getCategory());
            intent.putExtra(Constant.CONTENT_TYPE, item.getType());
            intent.putExtra(Constant.CONTENT_ID, item.getContentId());
            intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, item.getPremium());
            intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, item.getFreePlayTime());
            intent.putExtra(Constant.CONTENT_NAME, item.getTitle());
            startActivity(intent);

        } else {
            Toast.makeText(this, "Something went wrong check network connection",
                    Toast.LENGTH_LONG).show();
        }

    }

    private String getArtistName(String artist) {
        if (artist.contains(",")) {
            String[] parts = artist.split(",");
            artist = parts[0];
        }
        return artist;

    }

    private void shareAppLink() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Cinematic");
            String sAux = "\nLet me recommend you this application\n\n";

            sAux = sAux + " http://play.google.com/store/apps/details?id=" + this.getPackageName() + "\n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }


    @Override
    public void onRefresh() {

        if (NetUtil.isNetConnected(this)) {
            loadData(true);
        } else {
            loadData(false);
        }

      /*  Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });*/
    }

    @Override
    public void preparePlayback() {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    @SuppressWarnings("ReferenceEquality")
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        if (trackGroups != lastSeenTrackGroupArray) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            if (mappedTrackInfo != null) {
                if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                        == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                    showToast(R.string.error_unsupported_video);
                }
                if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO)
                        == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                    showToast(R.string.error_unsupported_audio);
                }
            }
            lastSeenTrackGroupArray = trackGroups;
        }
    }


    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                progressBar.setVisibility(View.VISIBLE);
                break;

            case Player.STATE_ENDED:
                break;

            case Player.STATE_IDLE:
                break;

            case Player.STATE_READY:
                isPlaying = true;
                progressBar.setVisibility(View.GONE);
                break;

            default:
                if (player != null)
                    player.stop();
                break;

        }

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Toast.makeText(this, "Can not play facing problem Please try again"
                , Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onVisibilityChange(int visibility) {

    }

    private void openCustomDialog(final String wifi, final Item item) {

        if (item == null)
            return;
        // custom dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.item_custom_dialog);
        //dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent;
                if (!TextUtils.isEmpty(wifi) && wifi.equalsIgnoreCase("Wifi")) {
                    intent = new Intent(DetailActivity.this, RegistrationActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, item.getFile());
                    intent.putExtra(Constant.CONTENT_TYPE, item.getType());
                    intent.putExtra(Constant.CONTENT_ID, item.getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, item.getPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, item.getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, item.getTitle());


                } else {
                    intent = new Intent(DetailActivity.this, SubscriptionActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, item.getFile());
                    intent.putExtra(Constant.CONTENT_TYPE, item.getType());
                    intent.putExtra(Constant.CONTENT_ID, item.getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, item.getPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, item.getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, item.getTitle());

                }

                startActivity(intent);
                finish();

            }
        });

        if (!this.isFinishing()) {
            dialog.show();
        }
    }


    private void openBLDialog() {

        if (item == null)
            return;
        // custom dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.item_custom_dialog);
        //dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        TextView textEnglish = (TextView) dialog.findViewById(R.id.text_english);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        textEnglish.setText(getResources().getString(R.string.sub_bl_message_eng));
        text.setText(getResources().getString(R.string.sub_bl_message_bng));
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (!this.isFinishing()) {
            dialog.show();
        }
    }


    private void initializePlayer() {

        // TrackSelection.Factory trackSelectionFactory = new RandomTrackSelection.Factory();

        boolean preferExtensionDecoders =
                getIntent().getBooleanExtra(PREFER_EXTENSION_DECODERS_EXTRA, false);
        @DefaultRenderersFactory.ExtensionRendererMode int extensionRendererMode =
                ((CinematicApp) getApplication()).useExtensionRenderers()
                        ? (preferExtensionDecoders ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        DefaultRenderersFactory renderersFactory =
                new DefaultRenderersFactory(this, extensionRendererMode);

        trackSelector = new DefaultTrackSelector(new DefaultBandwidthMeter());
        trackSelector.setParameters(trackSelectorParameters);

        lastSeenTrackGroupArray = null;

        DefaultDrmSessionManager<FrameworkMediaCrypto> drmSessionManager = null;

        player = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, drmSessionManager);
        player.addAnalyticsListener(new EventLogger(trackSelector));
        playerView.setPlayer(player);
        playerView.setPlaybackPreparer(this);

        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

        playerView.setPlayer(player);
        player.addListener(this);



/*        MediaSource mediaSource = new HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null);*/

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();


        //playContent("");
    }


    private void playContent(String url, boolean isPlay) {

        if (!TextUtils.isEmpty(url)) {
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getPackageName()));
            MediaSource mediaSource = new HlsMediaSource(Uri.parse(url),
                    dataSourceFactory, null, null);


            if (player == null)
                return;
            player.prepare(mediaSource);

            final boolean haveStartPosition = currentWindow != C.INDEX_UNSET;

            String playerTime = SharedPref.read(getContentId() + Constant.PLAY_BACK_POSITION);

            if (!TextUtils.isEmpty(playerTime)) {
                player.seekTo(currentWindow, Long.valueOf(playerTime));
            } else {
                player.seekTo(currentWindow, playbackPosition);
            }

            player.setPlayWhenReady(isPlay);



            /*TODO disable player istance*/
      /*  player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());*/


        }

    }

    private void sendUserActivity(String userId, String contentId, String category, String contentName) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendUserActivity(userId,
                contentId, category, contentName, "app");
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        // get response
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }


    private void sendContinueWatchMovie(String userId, String contentId,
                                        String playTime,
                                        String portrait, String contentLength,
                                        String playPercentDuration, String premium) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendContinueWatchMovie(userId,
                contentId, playTime, portrait, contentLength, playPercentDuration, premium);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        // get response
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void releasePlayer() {

        if (player != null) {
            if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {
                long playPercentDuration = 0;

                if (isPlaying) {
                    long contentDuration = player.getDuration();
                    long currentPosition = player.getCurrentPosition();
                    playPercentDuration = 100 - (contentDuration - currentPosition) / (1000 * 100);
                }

                sendContinueWatchMovie(SharedPref.read(Constant.USER_PHONE_NO), getContentId(),
                        String.valueOf(player.getCurrentPosition()),
                        item.getPortrait(), item.getLength(),
                        String.valueOf(playPercentDuration), item.getPremium());
            }
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
            releaseMediaDrm();

            SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
        }
    }


    private void releaseMediaDrm() {
        if (mediaDrm != null) {
            mediaDrm.release();
            mediaDrm = null;
        }
    }


    public void fastForward() {
        player.seekTo(
                Math.min(player.getCurrentPosition() + 15000, player.getDuration())
        );
    }

    public void fastRewind() {
        player.seekTo(
                Math.min(player.getCurrentPosition() - 15000, player.getDuration())
        );
    }

    private void releaseAllWhenBackPress(String freePlayTime) {

        releasePlayer();
        TimeTracker timeTracker;
        if (player != null)
            playbackPosition = player.getCurrentPosition();


        int totalTimeCount = db.getTime(getContentId()).getTime() + timeCount;
        if (!TextUtils.isEmpty(freePlayTime)) {
            if (Integer.valueOf(freePlayTime) > totalTimeCount) {
                timeTracker = new TimeTracker(getContentId(), totalTimeCount);
                db.updateNote(timeTracker);
            }
        }

        if (waitTimer != null) {
            waitTimer.cancel();
            waitTimer = null;
        }

        Intent intent = new Intent(this,
                MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        this.finish();
    }


    private void trackerSelector(View v) {
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo != null) {
            int rendererType = mappedTrackInfo.getRendererType(0);

            boolean allowAdaptiveSelections =
                    rendererType == C.TRACK_TYPE_VIDEO
                            || (rendererType == C.TRACK_TYPE_AUDIO
                            && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                            == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);
            Pair<AlertDialog, TrackSelectionView> dialogPair =
                    TrackSelectionView.getDialog(this, "Video Quality", trackSelector, 0);

            //TrackSelectionArray trackSelectionArray=mappedTrackInfo.getTrackGroups(0);


            dialogPair.first.setInverseBackgroundForced(true);
            dialogPair.second.setShowDisableOption(true);
            dialogPair.second.setAllowAdaptiveSelections(allowAdaptiveSelections);
            dialogPair.first.show();
        }


        PopupMenu popupMenu = new PopupMenu(this, v);
        Menu menu = popupMenu.getMenu();
        menu.add(Menu.NONE, 0, 0, "Video Quality");
        //player.get


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                DefaultTrackSelector.Parameters parameter = trackSelector.getParameters();
                return false;
            }
        });

    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }


    private void getOperator() {

        switch (NetUtil.getNetworkInfo(this)) {
            case "Mobile":
                getMSISDN();
                break;

            case "Wifi":
                if (item != null)
                    openCustomDialog("Wifi", item);
                break;
            default:
                break;
        }
    }


    private void getMSISDN() {

        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {
                            return;
                        } else {
                            String operatorName = operatorInfo.getOp();
                            String mobileNo = operatorInfo.getMobileNo();
                            SharedPref.write(Constant.USER_OPERATOR, operatorName);
                            SharedPref.write(Constant.USER_PHONE_NO, mobileNo);
                            isSubscribe(mobileNo, operatorName);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(DetailActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }


    private void checkBlDataConnection() {

        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {
                            return;
                        } else {
                            if (operatorInfo.getOp().equalsIgnoreCase("BDB111")) {
                                openCustomDialog("", item);
                            } else {
                              /*  Toast.makeText(DetailActivity.this, "Please Try From Baglalink Mobile Data",
                                        Toast.LENGTH_LONG).show();
                              */
                                openBLDialog();

                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(DetailActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }


    private void isSubscribe(String mobileNo, String op) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<SubResponse> call = jsonPostService.isSubscribe(mobileNo, op);
        call.enqueue(new Callback<SubResponse>() {

            @Override
            public void onResponse(@NonNull Call<SubResponse> call, @NonNull Response<SubResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String status = response.body().getStatus();
                        if (status.equals("1")) {
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                            if (!TextUtils.isEmpty(item.getFile()))
                                playContent(item.getFile(), true);


                        } else {
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "0");
                            if (player != null)
                                player.stop();
                            //openDialogForSub();
                            if (item != null) {
                                openCustomDialog("", item);
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<SubResponse> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

}
