package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContinueWatchItem {

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("playTime")
    @Expose
    private String playTime;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    @SerializedName("contentLength")
    @Expose
    private String contentLength;

    @SerializedName("playPercentDuration")
    @Expose
    private String playPercentDuration;

    @SerializedName("premium")
    @Expose
    private String premium;


    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getContentLength() {
        return contentLength;
    }

    public void setContentLength(String contentLength) {
        this.contentLength = contentLength;
    }

    public String getPlayPercentDuration() {
        return playPercentDuration;
    }

    public void setPlayPercentDuration(String playPercentDuration) {
        this.playPercentDuration = playPercentDuration;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }
}
