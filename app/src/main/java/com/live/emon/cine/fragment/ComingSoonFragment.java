package com.live.emon.cine.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.UpComingMoviesAdapter;
import com.live.emon.cine.helper.MyDividerItemDecoration;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.model.UpComingMovie;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.live.emon.cine.utils.SharedPref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ComingSoonFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {


    private List<Item> items = new ArrayList<>();
    private RecyclerView recyclerView;
    private UpComingMoviesAdapter mAdapter;
    private View view;
    private SwipeRefreshLayout swipeRefresh;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;

    public ComingSoonFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_coming_soon, container, false);
        initToolbar();
        intiViw();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
         /*Tovuti.from(getActivity()).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });*/

        if (NetUtil.isNetConnected(getActivity())) {
            loadData(true);
        } else {
            loadData(false);
        }
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");

        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(mToolbar);


        TextView tvToolbarFont = (TextView) view.findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }
        tvToolbarFont.setTypeface(font);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Tovuti.from(getActivity()).stop();
    }


    private void intiViw() {


        relativeLytNoNetwork = (RelativeLayout) view.findViewById(R.id.relative_lyt_no_network);

        imgNoNetLogo = (ImageView) view.findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = (TextView) view.findViewById(R.id.tv_something_wrong);
        tvfailedMessage = (TextView) view.findViewById(R.id.failed_message);

        btnFailedRetry = (Button) view.findViewById(R.id.btn_failed_retry);
        btnFailedRetry.setOnClickListener(this);


        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        int myColor = Color.parseColor("#494848");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.BLACK, Color.GRAY);


        swipeRefresh.setRefreshing(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.recy_view_coming_soon);
        mAdapter = new UpComingMoviesAdapter(getActivity(), items);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new MyDividerItemDecoration(2,
                Objects.requireNonNull(getActivity()).getResources().getColor(R.color.border_color)));
        recyclerView.setAdapter(mAdapter);

    }

    private void loadData(boolean isConnected) {
        if (isConnected) {
            swipeRefresh.setRefreshing(true);
            relativeLytNoNetwork.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            loadUpComingMovie(Constant.COMING_SOON);
        } else {
            recyclerView.setVisibility(View.GONE);
            swipeRefresh.setRefreshing(false);
            //  Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }


    private void loadUpComingMovie(String categoryName) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ItemResponse> call = apiService.getItemsByCategory(categoryName);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    List<Item> movies = response.body().getItems();
                    if (movies != null) {
                        mAdapter.clear();
                        items.addAll(movies);
                        mAdapter.notifyDataSetChanged();
                        swipeRefresh.setRefreshing(false);
                    }
                    Timber.v("Movies " + movies.toString());
                }


                //Log.d(TAG, "Number of Item Recived: " + movies.size());
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }


    @Override
    public void onRefresh() {
       /* Tovuti.from(getActivity()).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });
*/
        if (NetUtil.isNetConnected(getActivity())) {
            loadData(true);
        } else {
            loadData(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_failed_retry:
               /* Tovuti.from(getActivity()).monitor(new Monitor.ConnectivityListener() {
                    @Override
                    public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                        loadData(isConnected);
                    }
                });*/

                if (NetUtil.isNetConnected(getActivity())) {
                    loadData(true);
                } else {
                    loadData(false);
                }
                break;
        }
    }


}
