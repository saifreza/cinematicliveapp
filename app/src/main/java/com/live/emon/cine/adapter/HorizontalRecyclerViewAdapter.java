package com.live.emon.cine.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.live.emon.cine.R;
import com.live.emon.cine.activity.DetailActivity;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.Glider;

import java.util.List;


public class HorizontalRecyclerViewAdapter extends
        RecyclerView.Adapter<HorizontalRecyclerViewAdapter.HorizontalViewHolder> {

    private Context mContext;
    private List<Item> mArrayList;
    private static final int CONTINUE_WATCHING_MOVIE = 0;
    private static final int LATEST_MOVIE = 1;

    public HorizontalRecyclerViewAdapter(Context mContext,
                                         List<Item> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    public int getImage(String imageName) {
        return mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
    }

    @NonNull
    @Override
    public HorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_horizontal, parent, false);
        return new HorizontalViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull HorizontalViewHolder holder, int position) {

        final Item item = mArrayList.get(position);

        final String strTitle = item.getCategory();

        if (!TextUtils.isEmpty(strTitle)) {
            if (strTitle.equalsIgnoreCase("continue watching")) {
                holder.lytContinueWatch.setVisibility(View.VISIBLE);
                holder.tvTime.setText(item.getLength());
                holder.simpleProgressBar.setProgressTintList(ColorStateList.valueOf(Color.RED));
                holder.simpleProgressBar.setProgress(
                        Integer.valueOf(item.getPlayPercentDuration()));
            }

            else if(strTitle.equalsIgnoreCase("my list")){
                holder.lytContinueWatch.setVisibility(View.GONE);

            }

            else {
                holder.lytContinueWatch.setVisibility(View.GONE);
            }
        }


        holder.cardView.setTag(item);

        if (item.getPremium() != null) {
            if (item.getPremium().equalsIgnoreCase("1")) {
                holder.tvPremium.setVisibility(View.VISIBLE);
            } else {
                holder.tvPremium.setVisibility(View.GONE);
            }
        }


        /*  GlideApp.with(mContext)
                .load(current.getImagePath())
                .into(holder.imageView);

*/
        if (item.getThumbnail() != null)
            Glider.showWithPlaceholder(holder.imageView, item.getThumbnail());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, item.getPortrait(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mArrayList != null) {
            return mArrayList.size();
        } else
            return 0;

    }

    class HorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // TextView txtTitle;
        private ImageView imageView, ivError;
        private TextView tvPremium, tvTime;
        private CardView cardView;
        private LinearLayout lytContinueWatch;
        private ProgressBar simpleProgressBar;

        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/the real magazine demo.ttf");
        ;


        public HorizontalViewHolder(View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tv_time);
            ivError = itemView.findViewById(R.id.iv_error);
            lytContinueWatch = itemView.findViewById(R.id.lyt_continue_watch);
            simpleProgressBar = itemView.findViewById(R.id.simpleProgressBar);
            imageView = itemView.findViewById(R.id.ivThumb);
            tvPremium = itemView.findViewById(R.id.tv_premium);
            cardView = itemView.findViewById(R.id.lyt_main);
            cardView.setOnClickListener(this);
            /*tvPremium.setTypeface(font);
            tvPremium.setText("Premium");
            tvPremium.setTextColor(mContext.
                    getResources().
                    getColor(R.color.black));*/
            // itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Item item = (Item) v.getTag();
            if (item == null)
                return;
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra(Constant.CONTENT_ID, item.getContentId());
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        }
    }
}
