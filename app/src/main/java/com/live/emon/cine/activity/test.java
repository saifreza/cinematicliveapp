package com.live.emon.cine.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.live.emon.cine.R;

import java.util.HashMap;
import java.util.Map;

public class test extends AppCompatActivity {
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        webView = (WebView) findViewById(R.id.webView);

        // HttpGet request = new HttpGet("http://cinematic.mobi/DPDP/");

        //request.addHeader("Referer", "http://cinematic.mobi/DPDP/");

        //HttpResponse response = HttpClientBuilder.create().build().execute(request);

        // initWebView("http://cinematic.mobi/DPDP/");


      /*  webView.getSettings().setJavaScriptEnabled(true); // enable javascript

        final Activity activity = this;

        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        webView.loadUrl("http://cinematic.mobi/DPDP/");*/




        String url = "http://cinematic.mobi/DPDP/";

        Map<String, String> extraHeaders = new HashMap<String, String>();
        extraHeaders.put("Referer", "http://cinematic.mobi/DPDP/");


        webView.loadUrl(url, extraHeaders);

    }


    private void initWebView(String url) {
        webView = (WebView) findViewById(R.id.webView);
        webView.clearCache(true);
        webView.setWebViewClient(new test.MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        webView.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            String unSubUrl = url;
           /* unSubUrl = unSubUrl.substring(0, 29);
            if (!TextUtils.isEmpty(unSubUrl) && unSubUrl.equalsIgnoreCase(
                    "http://cinematic.mobi/app.php")) {
                SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");

            }*/
        }
    }

}
