package com.live.emon.cine.adapter;

import android.view.View;

public interface ItemClickListener<T>{
    void onItemClick(View view, T item);
}
