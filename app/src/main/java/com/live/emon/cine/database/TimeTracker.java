package com.live.emon.cine.database;

public class TimeTracker {

    public static final String TABLE_NAME = "time_tracker";

    public static final String CONTENT_ID = "content_id";
    public static final String CONTENT_TIME = "content_time";

    private String contentId;
    private int time;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + CONTENT_ID + " TEXT PRIMARY KEY,"
                    + CONTENT_TIME + " INTEGER"
                    + ")";


    public TimeTracker() {
    }

    public TimeTracker(String contentId, int time) {
        this.contentId = contentId;
        this.time = time;
    }


    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public int getTime() {
        return time;
    }


    public void setTime(int time) {
        this.time = time;
    }
}
