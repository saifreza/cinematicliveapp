package com.live.emon.cine.model;

import android.os.Parcel;
import android.os.Parcelable;


public abstract class BaseParcel extends BaseSerial implements Parcelable {

    protected BaseParcel() {}

    protected BaseParcel(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
