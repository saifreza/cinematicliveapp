package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HelpResponse {

    @SerializedName("Items")
    @Expose
    private List<ItemHelp> ItemHelp;

    public List<ItemHelp> getItemHelp() {
        return ItemHelp;
    }

    public void setItemHelp(List<ItemHelp> itemHelp) {
        ItemHelp = itemHelp;
    }

}
