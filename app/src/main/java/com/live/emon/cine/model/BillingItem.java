package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillingItem {

    @SerializedName("oparetorname")
    @Expose
    private String oparetorname;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("service")
    @Expose
    private String service;

    @SerializedName("apiSub")
    @Expose
    private String apiSub;

    @SerializedName("apiMessage")
    @Expose
    private String apiMessage;

    @SerializedName("apiBilling")
    @Expose
    private String apiBilling;

    @SerializedName("messageBody")
    @Expose
    private String messageBody;

    @SerializedName("billingAmount")
    @Expose
    private String billingAmount;


    @SerializedName("apiunSub")
    @Expose
    private String apiunSub;

    @SerializedName("loadSubApi")
    @Expose
    private String loadSubApi;

    @SerializedName("messageBN")
    @Expose
    private String messageBN;


    @SerializedName("messageEN")
    @Expose
    private String messageEN;


    public String getOparetorname() {
        return oparetorname;
    }

    public void setOparetorname(String oparetorname) {
        this.oparetorname = oparetorname;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }


    public String getApiSub() {
        return apiSub;
    }

    public void setApiSub(String apiSub) {
        this.apiSub = apiSub;
    }

    public String getApiMessage() {
        return apiMessage;
    }

    public void setApiMessage(String apiMessage) {
        this.apiMessage = apiMessage;
    }

    public String getApiBilling() {
        return apiBilling;
    }

    public void setApiBilling(String apiBilling) {
        this.apiBilling = apiBilling;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getBillingAmount() {
        return billingAmount;
    }

    public void setBillingAmount(String billingAmount) {
        this.billingAmount = billingAmount;
    }

    public String getApiunSub() {
        return apiunSub;
    }

    public void setApiunSub(String apiunSub) {
        this.apiunSub = apiunSub;
    }

    public String getLoadSubApi() {
        return loadSubApi;
    }

    public void setLoadSubApi(String loadSubApi) {
        this.loadSubApi = loadSubApi;
    }

    public String getMessageBN() {
        return messageBN;
    }

    public void setMessageBN(String messageBN) {
        this.messageBN = messageBN;
    }

    public String getMessageEN() {
        return messageEN;
    }

    public void setMessageEN(String messageEN) {
        this.messageEN = messageEN;
    }
}
