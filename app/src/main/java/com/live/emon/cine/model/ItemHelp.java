package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ItemHelp {

    @SerializedName("helpInfo")
    @Expose
    String helpInfo;

    public String getHelpInfo() {
        return helpInfo;
    }

    public void setHelpInfo(String helpInfo) {
        this.helpInfo = helpInfo;
    }

}
