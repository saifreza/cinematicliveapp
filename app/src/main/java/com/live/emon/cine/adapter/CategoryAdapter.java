package com.live.emon.cine.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.live.emon.cine.R;
import com.live.emon.cine.activity.MovieActivity;
import com.live.emon.cine.model.Category;
import com.live.emon.cine.utils.CaseConverter;
import com.live.emon.cine.utils.Constant;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private List<Category> categoryList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView categoryTitle;
        public RelativeLayout lytMain;

        public MyViewHolder(View view) {
            super(view);
            categoryTitle = (TextView) view.findViewById(R.id.tv_category_title);
            lytMain = (RelativeLayout) view.findViewById(R.id.lyt_main);
            lytMain.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Category category = (Category) v.getTag();
            if (category == null)
                return;
            Intent intent = new Intent(mContext, MovieActivity.class);
            intent.putExtra(Constant.CATEGORY_TITLE, category.getCategory());
            mContext.startActivity(intent);
        }
    }


    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.mContext = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Category category = categoryList.get(position);
        if (category == null)
            return;
        holder.lytMain.setTag(category);
        holder.categoryTitle.setText(CaseConverter.camelCase(category.getCategory()));

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

}
