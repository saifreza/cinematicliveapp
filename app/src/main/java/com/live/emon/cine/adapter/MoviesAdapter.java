package com.live.emon.cine.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.emon.cine.R;
import com.live.emon.cine.activity.DetailActivity;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.utils.Constant;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Item> moviesList;
    private Context mContext;
    private ItemClickListener itemClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, duration, genre, director;
        ImageView ivMovieThumb;
        ImageButton imageButtonPlay;
        RelativeLayout lytMain;

         MyViewHolder(View view) {
            super(view);
            title =  view.findViewById(R.id.title);
            genre =  view.findViewById(R.id.tv_genre);
            director =  view.findViewById(R.id.tv_directed_by);
            duration =  view.findViewById(R.id.tv_duration);
            ivMovieThumb =  view.findViewById(R.id.iv_movie);
            imageButtonPlay =  view.findViewById(R.id.image_button_play);
            lytMain =  view.findViewById(R.id.lyt_main);
            lytMain.setOnClickListener(this);
            imageButtonPlay.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Item item = (Item) v.getTag();
            if (item == null)
                return;
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra(Constant.CONTENT_ID, item.getContentId());
            mContext.startActivity(intent);
            /*TODO
             * Finished the activity*/
        }
    }


    public MoviesAdapter(Context context, List<Item> moviesList) {
        this.mContext = context;
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie_list, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item item = moviesList.get(position);
        holder.lytMain.setTag(item);
         holder.imageButtonPlay.setTag(item);
        holder.title.setText(item.getTitle());
        holder.genre.setText("Cast : " + item.getArtist());
        holder.director.setText("Director : " + item.getDirector());
        holder.duration.setText("Duration : " +item.getLength());

        Glide.with(mContext)
                .load(item.getLandscape())
                .into(holder.ivMovieThumb);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public int getImage(String imageName) {

        return mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
    }
}
