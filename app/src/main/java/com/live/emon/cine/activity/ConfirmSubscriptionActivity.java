package com.live.emon.cine.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.emon.cine.R;
import com.live.emon.cine.model.BillingItem;
import com.live.emon.cine.model.BillingItemResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.Glider;
import com.live.emon.cine.utils.SharedPref;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ConfirmSubscriptionActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivOperator;
    private Button btnCardPayment, btnMobilePayment;
    private String subApi = "";
    private String loadSubApi = "";
    private String apiMessage = "";
    private String apiBilling = "";
    private String messageBody = "";
    private String billingAmount = "";
    private FirebaseAnalytics mFirebaseAnalytics;
    private ProgressBar progressBar;
    private int count = 0;
    private WebView webView;
    private LinearLayout lytMobileBilling;
    private LinearLayout lytWebView;
    private TextView tvPaymentBangla, tvPaymentEnglish;
    private ImageView ivBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_subscripton);
        initToolbar();
        initView();
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

   /* public void setExternalFonts(EditText tv) {
        Typeface tf = Typeface.createFromAsset(this.getAssets(),
                "fonts/SUTOM___.TTF");
        tv.setTypeface(tf);
        tv.setHint(this.getResources().getString(R.string.pay_bill_info));
    }*/


    private void initWebView(String url) {
        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new ConfirmSubscriptionActivity.MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    private void initToolbar() {
        android.support.v7.widget.Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setText(getString(R.string.app_name));
        tvToolbarFont.setTypeface(font);
    }

    private void initView() {
        ivOperator = (ImageView) findViewById(R.id.iv_operator);
        ivBanner = (ImageView) findViewById(R.id.iv_banner);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        // btnCardPayment = (Button) findViewById(R.id.btn_card_payment);
        lytMobileBilling = (LinearLayout) findViewById(R.id.lyt_mobile_billing);
        lytWebView = (LinearLayout) findViewById(R.id.lyt_webView);
        btnMobilePayment = (Button) findViewById(R.id.btn_subscribe);
        tvPaymentBangla = findViewById(R.id.tv_payment_bangla);
        tvPaymentEnglish = findViewById(R.id.tv_payment_bangla);
        //  btnCardPayment.setOnClickListener(this);
        btnMobilePayment.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                internetChecking(isConnected);
            }
        });

        btnMobilePayment.setClickable(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.btn_card_payment:
                Intent cardPayment = new Intent(this, CardPaymentActivity.class);
                startActivity(cardPayment);
                break;*/

            case R.id.btn_subscribe:

                logForFirebase();
                if (TextUtils.isEmpty(subApi)) {
                    Toast.makeText(this, "Something went wrong please try again"
                            , Toast.LENGTH_LONG).show();
                } else {
                    if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))) {

                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.setProgress(100);
                        btnMobilePayment.setClickable(false);
                        btnMobilePayment.setBackgroundColor(getResources().getColor(R.color.light_gray));
                        String baseUrl = subApi;
                        apiSubscription(baseUrl);

                    } else {
                        Toast.makeText(this, "Please Login First"
                                , Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
             /*   item.setIcon(R.drawable.ic_left_arrow);
                Intent intent = new Intent(this,
                        MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
*/
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void apiMessage() {
        if (!TextUtils.isEmpty(apiMessage)) {
            String baseUrl = apiMessage;
            apiMessage(baseUrl);
        }
    }

    private void apiBilling() {
        if (!TextUtils.isEmpty(apiBilling)) {
            String baseUrl = apiBilling;
            apiBilling(baseUrl);
        }
    }

    private void logForFirebase() {
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.ITEM_LIST, ++count);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SharedPref.read(Constant.USER_NAME));
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SharedPref.read(Constant.USER_PHONE_NO));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, bundle);
    }

    private void internetChecking(boolean isConnected) {
        if (isConnected) {
            if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))
                    && !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
                getBillingInfo(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));
            }


        } else {
            Toast.makeText(ConfirmSubscriptionActivity.this, "No Network Found",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void getBillingInfo(String operator, String mobileNo) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<BillingItemResponse> call = jsonPostService.getBillingInfo(operator, mobileNo, "app");
        call.enqueue(new Callback<BillingItemResponse>() {

            @Override
            public void onResponse(@NonNull Call<BillingItemResponse> call, @NotNull Response<BillingItemResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        BillingItem billingItem = response.body().getItems();
                        if (billingItem == null)
                            return;
                        else {
                            if (!TextUtils.isEmpty(billingItem.getApiSub())) {


                                subApi = billingItem.getApiSub();
                                loadSubApi = billingItem.getLoadSubApi();
                                apiMessage = billingItem.getApiMessage();
                                apiBilling = billingItem.getApiBilling();
                                messageBody = billingItem.getMessageBody();
                                billingAmount = billingItem.getBillingAmount();
                                String service = billingItem.getService();
                                String message = billingItem.getMessage();
                                String messageBN = billingItem.getMessageBN();
                                String messageEN = billingItem.getMessageEN();
                                Glider.loadImage(ivOperator, billingItem.getLogo());

                                Intent intent = new Intent();
                                intent.putExtra(Constant.SUB_API, subApi);
                                intent.putExtra(Constant.LOAD_SUB_API, loadSubApi);
                                intent.putExtra(Constant.API_MESSAGE, apiMessage);
                                intent.putExtra(Constant.API_BILLING, apiBilling);
                                intent.putExtra(Constant.MESSAGE_BODY, messageBody);
                                intent.putExtra(Constant.BILLING_AMOUNT, billingAmount);
                                intent.putExtra(Constant.SERVICE, service);
                                intent.putExtra(Constant.MESSAGE, message);
                                intent.putExtra(Constant.MESSAGE_BN, messageBN);
                                intent.putExtra(Constant.MESSAGE_EN, messageEN);
                                intent.putExtra(Constant.LOGO, billingItem.getLogo());
                                intent.putExtra(Constant.OPERATOR_NAME, billingItem.getOparetorname());


                                if (!TextUtils.isEmpty(service)) {
                                    if (service.equals("no")) {
                                        btnMobilePayment.setVisibility(View.GONE);
                                        tvPaymentBangla.setText(message);
                                        return;
                                    }
                                }

                                if (!TextUtils.isEmpty(loadSubApi)) {
                                    if (loadSubApi.equals("false")) {
                                        btnMobilePayment.setVisibility(View.VISIBLE);
                                    } else {
                                        lytMobileBilling.setVisibility(View.GONE);
                                        lytWebView.setVisibility(View.VISIBLE);
                                        initWebView(subApi);
                                    }
                                }


                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<BillingItemResponse> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    @Override
    protected void onStop() {
        Tovuti.from(this).stop();
        super.onStop();
    }

    private void apiSubscription(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String string = response.body();
                        if (string.equals("1")) {
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                            apiBilling();
                            apiMessage();
                            Toast.makeText(ConfirmSubscriptionActivity.this, "Subscription Complete", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ConfirmSubscriptionActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                            //btnMobilePayment.setBackgroundColor(R.color.white);
                            // btnMobilePayment.setOnClickListener(null);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ConfirmSubscriptionActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
                btnMobilePayment.setClickable(true);
                btnMobilePayment.setBackgroundColor(getResources().getColor(R.color.red));


            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ConfirmSubscriptionActivity.this, "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void apiMessage(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ConfirmSubscriptionActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
                Toast.makeText(ConfirmSubscriptionActivity.this, "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void apiBilling(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ConfirmSubscriptionActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
            }
        });
    }
}