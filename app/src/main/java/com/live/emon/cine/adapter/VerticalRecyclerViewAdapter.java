package com.live.emon.cine.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.live.emon.cine.R;
import com.live.emon.cine.helper.MyCustomLayoutManager;
import com.live.emon.cine.model.Category;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.utils.CaseConverter;

import java.util.ArrayList;
import java.util.List;


public class VerticalRecyclerViewAdapter extends
        RecyclerView.Adapter<VerticalRecyclerViewAdapter.VerticalRecyclerViewHolder> {

    private Context mContext;
    private List<Category> mArrayList = new ArrayList<>();

    public VerticalRecyclerViewAdapter(Context mContext, List<Category> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public VerticalRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vertical, parent, false);
        return new VerticalRecyclerViewHolder(view);
    }

    public void setList(List<Category> mArrayList) {
        this.mArrayList.addAll(mArrayList);
        notifyDataSetChanged();
    }

    public List<Category> getmArrayList() {
        return mArrayList;

    }

    public void clear() {
        final int size = mArrayList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                mArrayList.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onBindViewHolder(VerticalRecyclerViewHolder holder, int position) {

        final Category current = mArrayList.get(position);

        final String strTitle = current.getCategory();

        List<Item> singleSectionItems = current.getArrayList();


        if (singleSectionItems != null) {

            if (singleSectionItems.size() != 0) {

                holder.tvTitle.setText(CaseConverter.camelCase(strTitle));
                holder.lytMain.setVisibility(View.VISIBLE);
                HorizontalRecyclerViewAdapter itemListDataAdapter =
                        new HorizontalRecyclerViewAdapter(mContext, singleSectionItems);

                holder.rvHorizontal.setHasFixedSize(true);
                MyCustomLayoutManager mLayoutManager = new MyCustomLayoutManager(mContext);
                mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mLayoutManager.setReverseLayout(false);
                holder.rvHorizontal.setLayoutManager(mLayoutManager);
                holder.rvHorizontal.smoothScrollToPosition(0);

               /* holder.rvHorizontal.setLayoutManager(new LinearLayoutManager(mContext,
                        LinearLayoutManager.HORIZONTAL, false));
               */
                holder.rvHorizontal.setAdapter(itemListDataAdapter);
                holder.rvHorizontal.setNestedScrollingEnabled(false);

            }

        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class VerticalRecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        RecyclerView rvHorizontal;
        LinearLayout lytMain;

        public VerticalRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            rvHorizontal = itemView.findViewById(R.id.rvHorizontal);
            lytMain = itemView.findViewById(R.id.lyt_mainMain);

        }
    }
}
