package com.live.emon.cine.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.emon.cine.R;
import com.live.emon.cine.activity.DetailActivity;
import com.live.emon.cine.activity.PlayerActivity;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.Glider;

import java.util.List;

public class UpComingMoviesAdapter extends RecyclerView.Adapter<UpComingMoviesAdapter.MyViewHolder> {

    private List<Item> moviesList;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, date, description, tvDescription;
        ImageView ivUpcomingMovieThumb;
        CardView container;

        MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_upcoming_movie_title);
            //date = (TextView) view.findViewById(R.id.tv_date);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            ivUpcomingMovieThumb = (ImageView) view.findViewById(R.id.imv_upcoming_movie);
            container = (CardView) view.findViewById(R.id.container);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Item item = (Item) v.getTag();
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra(Constant.CONTENT_URL,  item.getFile());
            intent.putExtra(Constant.CONTENT_CATEGORY, item.getCategory());
            intent.putExtra(Constant.CONTENT_ID, item.getContentId());
            intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, item.getPremium());
            intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, item.getFreePlayTime());
            mContext.startActivity(intent);
        }
    }

    public void clear() {
        final int size = moviesList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                moviesList.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }


    public UpComingMoviesAdapter(Context context, List<Item> moviesList) {
        this.mContext = context;
        this.moviesList = moviesList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_coming_soon, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        // holder.description.setText(movie.getGenre());
        //holder.date.setText("Coming Up On : " + movie.getYear());
        holder.tvDescription.setText(movie.getDescription());
        holder.container.setTag(movie);
        Glider.showWithPlaceholder(holder.ivUpcomingMovieThumb, movie.getLandscape());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private int getImage(String imageName) {

        return mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
    }
}
