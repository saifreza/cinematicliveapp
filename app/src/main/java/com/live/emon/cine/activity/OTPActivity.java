package com.live.emon.cine.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.model.SubResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class OTPActivity extends AppCompatActivity
        implements View.OnClickListener, OnSmsCatchListener {
    private Button btnDone, btnResend;
    private EditText edtOpt;
    private TextView tvCountDown;
    int i = 0, second = 120;
    private SmsVerifyCatcher smsVerifyCatcher;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opt);
        initView();
        smsVerifyCatcher = new SmsVerifyCatcher(this, this);

    }

    private int getIntentValue() {
        return getIntent().getIntExtra(Constant.SIGN_IN, 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        countDownTime();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void countDownTime() {
        new Thread(new Runnable() {
            public void run() {
                while (i != second) {
                    try {
                        handle.sendMessage(handle.obtainMessage());
                        if (!TextUtils.isEmpty(edtOpt.getText())) {
                            tvCountDown.setVisibility(View.GONE);
                            btnResend.setVisibility(View.GONE);
                            Thread.interrupted();
                        }
                        Thread.sleep(1000);
                    } catch (Throwable t) {

                    }
                }


            }

            Handler handle = new Handler() {
                public void handleMessage(Message msg) {
                    tvCountDown.setText("Please Wait : " + second + "s");
                    second--;
                    if (second == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnResend.setVisibility(View.VISIBLE);
                                btnDone.setVisibility(View.GONE);
                                tvCountDown.setVisibility(View.GONE);
                            }
                        });

                        Thread.interrupted();
                    }
                }
            };
        }).start();

    }

    private void initView() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }

        btnDone = (Button) findViewById(R.id.btn_done);
        edtOpt = (EditText) findViewById(R.id.edt_otp);
        tvCountDown = (TextView) findViewById(R.id.tv_countdown);
        btnResend = (Button) findViewById(R.id.btn_resend);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        btnDone.setOnClickListener(this);
        btnResend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String otp = edtOpt.getText().toString();
        switch (v.getId()) {
            case R.id.btn_done:
                if (!TextUtils.isEmpty(otp)) {
                    btnDone.setClickable(false);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setProgress(100);
                    Toast.makeText(OTPActivity.this, "Please Wait",
                            Toast.LENGTH_SHORT).show();

                    if (!TextUtils.isEmpty(userPhoneNo()) &&
                            !TextUtils.isEmpty(otp)) {

                        @SuppressLint("HardwareIds") String deviceId = Settings.Secure.
                                getString(OTPActivity.this.getContentResolver(),
                                        Settings.Secure.ANDROID_ID);

                        validOtp(deviceId, userPhoneNo(), otp);
                    }
                } else {
                    Toast.makeText(OTPActivity.this, "Enter The Otp Number",
                            Toast.LENGTH_SHORT).show();

                }
                break;

            case R.id.btn_resend:
                btnDone.setClickable(true);
                if (!TextUtils.isEmpty(userPhoneNo())) {
                    requestAgainForOtp();
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void requestAgainForOtp() {
        btnResend.setVisibility(View.GONE);
        tvCountDown.setVisibility(View.VISIBLE);
        btnDone.setVisibility(View.VISIBLE);
        second = 120;

        new Thread(new Runnable() {
            public void run() {
                while (i != second) {
                    try {
                        handle.sendMessage(handle.obtainMessage());
                        if (!TextUtils.isEmpty(edtOpt.getText())) {
                            tvCountDown.setVisibility(View.GONE);
                            btnResend.setVisibility(View.GONE);
                            Thread.interrupted();
                        }
                        Thread.sleep(1000);
                    } catch (Throwable t) {
                    }
                }


            }

            Handler handle = new Handler() {
                public void handleMessage(Message msg) {
                    tvCountDown.setText("Please Wait : " + second + "s");
                    second--;
                    if (second == 0) {
                        btnResend.setVisibility(View.VISIBLE);
                        btnDone.setVisibility(View.GONE);
                        tvCountDown.setVisibility(View.GONE);
                    }
                }
            };
        }).start();

        requestForOtp(userName(), userPhoneNo(), SharedPref.read(Constant.USER_OPERATOR));
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    private void requestForOtp(final String deviceId, final String mobileNo, String operator) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendUserInfo(deviceId,
                mobileNo, operator);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String responseText = response.body();
                        if (responseText.equals("Accepted")) {
                            Toast.makeText(OTPActivity.this, "Check Your OTP Number In SMS",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OTPActivity.this, responseText,
                                    Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private String userName() {
        String userName = "";
        if (getIntent() != null) {
            userName = getIntent().getStringExtra(Constant.USER_NAME);
        }
        return userName;
    }

    private String userPhoneNo() {
        String userPhone = "";
        if (getIntent() != null) {
            userPhone = getIntent().getStringExtra(Constant.USER_PHONE_NO);
        }
        return userPhone;
    }


    private void validOtp(final String deviceId, final String mobileNo, final String otp) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.otpValidation(deviceId,
                mobileNo, otp);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String responseText = response.body();
                        if (responseText.equals("Success")) {

                            Toast.makeText(OTPActivity.this, " Login Successfully Complete",
                                    Toast.LENGTH_SHORT).show();
                            SharedPref.write(Constant.USER_PHONE_NO, mobileNo);

                            if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDR111")
                                    || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDA111")
                                    || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDG111")
                                    || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDB111"))
                                isSubscribe(SharedPref.read(Constant.USER_PHONE_NO),
                                        SharedPref.read(Constant.USER_OPERATOR));
                        } else {
                            Toast.makeText(OTPActivity.this, responseText,
                                    Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    btnDone.setClickable(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private void isSubscribe(final String mobileNo, String op) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<SubResponse> call = jsonPostService.isSubscribe(mobileNo, op);
        call.enqueue(new Callback<SubResponse>() {

            @Override
            public void onResponse(@NonNull Call<SubResponse> call, @NonNull Response<SubResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String status = response.body().getStatus();
                        SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS,
                                status);
                        if (status.equalsIgnoreCase("1")) {
                            startActivity(new Intent(OTPActivity.this,
                                    MainActivity.class));
                            finish();
                        } else {

                            Intent intent = new Intent(OTPActivity.this,
                                    SubscriptionActivity.class);
                            intent.putExtra(Constant.CONTENT_ID, getContentId());
                            startActivity(intent);

                            finish();
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<SubResponse> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private String getContentId() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_ID);
        return url;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onSmsCatch(String message) {
        String code = parseCode(message);//Parse verification code
        edtOpt.setText(code);//set code in edit text
        btnResend.setVisibility(View.GONE);
        tvCountDown.setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                item.setIcon(R.drawable.ic_left_arrow);
               /* Intent intent = new Intent(this,
                        RegistrationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();*/
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
