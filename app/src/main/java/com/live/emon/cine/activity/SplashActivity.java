package com.live.emon.cine.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.androidstudy.networkmanager.internal.NetworkUtil;
import com.live.emon.cine.R;
import com.live.emon.cine.listner.ImageManager;
import com.live.emon.cine.model.OperatorInfo;
import com.live.emon.cine.model.SubResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import java.util.*;


public class SplashActivity extends Activity implements View.OnClickListener {

    private ShimmerTextView shimmertvAppName;
    private Shimmer shimmer;
    private TextView tvToolbarFont;
    private Typeface font;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;
    private Thread thread;
    private volatile boolean running = true;
    private Tovuti tovuti;
    private WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
        ImageManager.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //  internetChecking(true);

        toggleAnimation(shimmertvAppName);


        if (NetUtil.isNetConnected(this)) {
            internetChecking(true);
        } else {
            internetChecking(false);
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        shimmer.cancel();

    }


    private void initView() {

        shimmertvAppName = (ShimmerTextView) findViewById(R.id.tv_app_name);
        font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        shimmertvAppName.setTypeface(font);


        relativeLytNoNetwork = findViewById(R.id.relative_lyt_no_network);
        imgNoNetLogo = findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = findViewById(R.id.tv_something_wrong);
        tvfailedMessage = findViewById(R.id.failed_message);


        btnFailedRetry = findViewById(R.id.btn_failed_retry);
        btnFailedRetry.setOnClickListener(this);

       /* tvItemMessage = (TextView) findViewById(R.id.no_item_message);
        tvItemMessage.setOnClickListener(this);
       */

        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                shimmertvAppName.setText("Banglalink \n Cinematic");

        } else {
            shimmertvAppName.setText(getString(R.string.app_name));
        }
        //shimmertvAppName.setText(getString(R.string.app_name));


    }


    private void internetChecking(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);

            CountDownTimer countDownTimer = new CountDownTimer(1000, 700) {

                @Override
                public void onFinish() {

                    thread = new Thread() {

                        @Override
                        public void run() {
                            if (!running) return;
                            getOperator();
                        }
                    };
                    thread.start();
                }

                @Override
                public void onTick(long millisUntilFinished) {

                }
            }.start();




        } else {
            //    Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }


    private void toggleAnimation(ShimmerTextView target) {
        if (shimmer != null && shimmer.isAnimating()) {
            shimmer.cancel();
        } else {
            shimmer = new Shimmer();
            shimmer.start(target);
            shimmer.setDuration(1000)
                    .setDirection(Shimmer.ANIMATION_DIRECTION_LTR);
        }
    }


    private void getOperator() {

        switch (NetUtil.getNetworkInfo(this)) {
            case "Mobile":
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO)) &&
                        !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
                    if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDR111")
                            || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDA111")
                            || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDG111")
                            || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDB111"))
                        isSubscribe(SharedPref.read(Constant.USER_PHONE_NO),
                                SharedPref.read(Constant.USER_OPERATOR));
                } else {
                    getMSISDN();
                }

                break;
            case "Wifi":
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO)) &&
                        !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
                    isSubscribe(SharedPref.read(Constant.USER_PHONE_NO), SharedPref.read(Constant.USER_OPERATOR));
                } else {
                    running = false;
                    goToMainActivity();
                }
                break;
            default:

                break;
        }
    }


    private void initWebView() {
        webView = (WebView) findViewById(R.id.webView_splash);
        webView.clearCache(true);
        webView.setWebViewClient(new SplashActivity.MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        String url = "http://cinematic.mobi/DPDP/";

        Map<String, String> extraHeaders = new HashMap<String, String>();
        extraHeaders.put("Referer", "http://cinematic.mobi/DPDP/");

        webView.loadUrl(url, extraHeaders);
    }


    private void getMSISDN() {

        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {
                            return;
                        } else {
                            String operatorName = operatorInfo.getOp();
                            String mobileNo = operatorInfo.getMobileNo();
                            if (operatorName.equalsIgnoreCase("BDG111")) {
                                SharedPref.write(Constant.USER_OPERATOR, operatorName);
                                initWebView();
                            } else {
                                SharedPref.write(Constant.USER_OPERATOR, operatorName);
                                SharedPref.write(Constant.USER_PHONE_NO, mobileNo);
                                isSubscribe(mobileNo, operatorName);
                            }

                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(SplashActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }

    private void isSubscribe(String mobileNo, String op) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<SubResponse> call = jsonPostService.isSubscribe(mobileNo, op);
        call.enqueue(new Callback<SubResponse>() {

            @Override
            public void onResponse(@NonNull Call<SubResponse> call, @NonNull Response<SubResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String status = response.body().getStatus();
                        SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS,
                                status);
                        running = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<SubResponse> call, @NonNull Throwable t) {
                Toast.makeText(SplashActivity.this, "No Network Found",
                        Toast.LENGTH_SHORT).show();
            }
        });

        goToMainActivity();
    }

    private void goToMainActivity() {
        Intent intent = new Intent(
                SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_failed_retry:
                if (NetUtil.isNetConnected(this)) {
                    internetChecking(true);
                } else {
                    internetChecking(false);
                }
                break;
        }
    }


    private class MyBrowser extends WebViewClient {
       /* @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }*/

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.contains("endUserId=")) {
                String endUserId = deleteAllNonDigit(url);
                SharedPref.write(Constant.USER_PHONE_NO, endUserId);
                isSubscribe(endUserId, "BDG111");
            }

        }

        private String deleteAllNonDigit(String s) {
            return s.replaceAll("\\D", "");
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            String unSubUrl = url;
           /* unSubUrl = unSubUrl.substring(0, 29);
            if (!TextUtils.isEmpty(unSubUrl) && unSubUrl.equalsIgnoreCase(
                    "http://cinematic.mobi/app.php")) {
                SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");

            }*/
        }
    }
}
