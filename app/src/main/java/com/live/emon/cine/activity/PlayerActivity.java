package com.live.emon.cine.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Pair;
import android.view.*;
import android.widget.*;

import com.google.android.exoplayer2.*;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.TrackSelectionView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.live.emon.cine.R;
import com.live.emon.cine.app.CinematicApp;
import com.live.emon.cine.database.DatabaseHelper;
import com.live.emon.cine.database.TimeTracker;
import com.live.emon.cine.model.OperatorInfo;
import com.live.emon.cine.model.SubResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;

import java.lang.reflect.Parameter;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class PlayerActivity extends AppCompatActivity
        implements View.OnClickListener, Player.EventListener,
        PlayerControlView.VisibilityListener, PlaybackPreparer {

    private DatabaseHelper db;
    public static final String PREFER_EXTENSION_DECODERS_EXTRA = "prefer_extension_decoders";

    private PlayerView playerView;
    private SimpleExoPlayer player;
    private ProgressBar progressBar;
    private boolean playWhenReady;
    private int currentWindow;
    private long playbackPosition;
    private FrameworkMediaDrm mediaDrm;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private FirebaseAnalytics mFirebaseAnalytics;
    //private boolean playerState;
    private int timeCount = 0;
    private CountDownTimer waitTimer;
    private MediaSource mediaSource;
    private TrackGroupArray lastSeenTrackGroupArray = null;
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_player);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        db = new DatabaseHelper(this);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        if (!TextUtils.isEmpty(getContentId())) {

            List<TimeTracker> timeTrackers = db.getAllNotes();

            if (timeTrackers.size() != 0) {
                for (TimeTracker timeTracker : timeTrackers) {
                    // find db contentID
                    String contentId = timeTracker.getContentId();
                    if (!contentId.equals(getContentId())) {
                        db.insertTime(new TimeTracker(getContentId(), 0));
                        break;
                    }
                }
            } else {
                db.insertTime(new TimeTracker(getContentId(), 0));
            }
        }

        initView();
        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            //startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            // startWindow = savedInstanceState.getInt(KEY_WINDOW);
            // startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().build();
            // clearStartPosition();
        }
        // playerState = true;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
    }

    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }

    private void logForFireBase() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_LIST, getContentName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SharedPref.read(Constant.USER_NAME));
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, SharedPref.read(Constant.USER_PHONE_NO));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }


    private void openDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Please Sign In or Sign Up" +
                "To Continue This Video");
        alertDialogBuilder.setPositiveButton("Sign In",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(PlayerActivity.this, SignInActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton("Sign Up", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(PlayerActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void openDialogForSub() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("To Continue This Video Please Click OK ");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(PlayerActivity.this, SubscriptionActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void openCustomDialog(final String wifi) {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.item_custom_dialog);
        //dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent;
                if (!TextUtils.isEmpty(wifi) && wifi.equalsIgnoreCase("Wifi")) {
                    intent = new Intent(PlayerActivity.this, RegistrationActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, getUrl());
                    intent.putExtra(Constant.CONTENT_TYPE, getContentType());
                    intent.putExtra(Constant.CONTENT_ID, getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, isPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, getContentName());

                } else {
                    intent = new Intent(PlayerActivity.this, SubscriptionActivity.class);
                    intent.putExtra(Constant.CONTENT_URL, getUrl());
                    intent.putExtra(Constant.CONTENT_TYPE, getContentType());
                    intent.putExtra(Constant.CONTENT_ID, getContentId());
                    intent.putExtra(Constant.CONTENT_PREMIUM_STATUS, isPremium());
                    intent.putExtra(Constant.CONTENT_FREE_PLAY_TIME, getFreePlayTime());
                    intent.putExtra(Constant.CONTENT_NAME, getContentName());

                }

                startActivity(intent);
                finish();
                // dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void initView() {
        ImageView ivPlayButton = findViewById(R.id.exo_play);
        ImageView ivPauseButton = findViewById(R.id.exo_pause);
        ImageView ivFastForwardButton = findViewById(R.id.exo_ffwd);
        ImageView ivReWindButton = findViewById(R.id.exo_rew);
        //ImageView ivSetting = findViewById(R.id.exo_setting);
        ImageView ivBack = findViewById(R.id.iv_back);
        FrameLayout lytBack = findViewById(R.id.lyt_back);

        ivPlayButton.setOnClickListener(this);
        ivPauseButton.setOnClickListener(this);
        ivFastForwardButton.setOnClickListener(this);
        ivReWindButton.setOnClickListener(this);
        //ivSetting.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        lytBack.setOnClickListener(this);

        progressBar = findViewById(R.id.progress_bar);

        playerView = findViewById(R.id.player_view);

        playerView.setControllerVisibilityListener(this);
        playerView.requestFocus();
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

    }

    private String getUrl() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_URL);
        return url;
    }

    private String isPremium() {
        String premium = "";
        Intent intent = getIntent();
        if (intent != null)
            premium = intent.getStringExtra(Constant.CONTENT_PREMIUM_STATUS);
        return premium;
    }


    private String getContentName() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_NAME);
        return url;
    }

    private String getContentId() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_ID);
        return url;
    }

    private String getContentType() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_TYPE);
        return url;
    }

    private String getFreePlayTime() {
        String time = "";
        Intent intent = getIntent();
        if (intent != null)
            time = intent.getStringExtra(Constant.CONTENT_FREE_PLAY_TIME);
        return time;
    }

    private void initializePlayer() {

        // TrackSelection.Factory trackSelectionFactory = new RandomTrackSelection.Factory();

        boolean preferExtensionDecoders =
                getIntent().getBooleanExtra(PREFER_EXTENSION_DECODERS_EXTRA, false);
        @DefaultRenderersFactory.ExtensionRendererMode int extensionRendererMode =
                ((CinematicApp) getApplication()).useExtensionRenderers()
                        ? (preferExtensionDecoders ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        DefaultRenderersFactory renderersFactory =
                new DefaultRenderersFactory(this, extensionRendererMode);

        trackSelector = new DefaultTrackSelector(new DefaultBandwidthMeter());
        trackSelector.setParameters(trackSelectorParameters);

        lastSeenTrackGroupArray = null;

        DefaultDrmSessionManager<FrameworkMediaCrypto> drmSessionManager = null;

        player =
                ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, drmSessionManager);
        player.addAnalyticsListener(new EventLogger(trackSelector));
        playerView.setPlayer(player);
        playerView.setPlaybackPreparer(this);

        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

        playerView.setPlayer(player);
        player.addListener(this);



/*        MediaSource mediaSource = new HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null);*/

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getPackageName()));
        if (TextUtils.isEmpty(getUrl()))
            return;
        mediaSource = new HlsMediaSource(Uri.parse(getUrl()),
                dataSourceFactory, null, null);


        /*TODO disable player istance*/
      /*  player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());*/

        playContent(mediaSource);
    }


    private void playContent(MediaSource mediaSource) {

        player.prepare(mediaSource);

        final boolean haveStartPosition = currentWindow != C.INDEX_UNSET;

        String playerTime = SharedPref.read(getContentId() + Constant.PLAY_BACK_POSITION);

        if (!TextUtils.isEmpty(playerTime)) {
            player.seekTo(currentWindow, Long.valueOf(playerTime));
        } else {
            player.seekTo(currentWindow, playbackPosition);
        }

        /*if (haveStartPosition) {
            player.seekTo(currentWindow, playbackPosition);
        } else {
            player.seekTo(currentWindow, playbackPosition);
        }*/
        //player.prepare(mediaSource, !haveStartPosition, false);

        player.setPlayWhenReady(true);
    }


    private void sendUserActivity(String userId, String contentId, String category, String contentName) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendUserActivity(userId,
                contentId, category, contentName, "app");
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        // get response
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }


    private DefaultDrmSessionManager<FrameworkMediaCrypto> buildDrmSessionManagerV18(
            UUID uuid, String licenseUrl, String[] keyRequestPropertiesArray, boolean multiSession)
            throws UnsupportedDrmException {
        HttpDataSource.Factory licenseDataSourceFactory =
                ((CinematicApp) getApplication()).buildHttpDataSourceFactory(
                        null);
        HttpMediaDrmCallback drmCallback =
                new HttpMediaDrmCallback(licenseUrl, licenseDataSourceFactory);
        if (keyRequestPropertiesArray != null) {
            for (int i = 0; i < keyRequestPropertiesArray.length - 1; i += 2) {
                drmCallback.setKeyRequestProperty(keyRequestPropertiesArray[i],
                        keyRequestPropertiesArray[i + 1]);
            }
        }
        releaseMediaDrm();
        mediaDrm = FrameworkMediaDrm.newInstance(uuid);
        return new DefaultDrmSessionManager<>(uuid, mediaDrm, drmCallback, null, multiSession);
    }

    private void releaseMediaDrm() {
        if (mediaDrm != null) {
            mediaDrm.release();
            mediaDrm = null;
        }
    }


    public void fastForward() {
        player.seekTo(
                Math.min(player.getCurrentPosition() + 15000, player.getDuration())
        );
    }

    public void fastRewind() {
        player.seekTo(
                Math.min(player.getCurrentPosition() - 15000, player.getDuration())
        );
    }


    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void releasePlayer() {

        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
            releaseMediaDrm();

            SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
        }
    }


    private void isSubscribe(String mobileNo, String op) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<SubResponse> call = jsonPostService.isSubscribe(mobileNo, op);
        call.enqueue(new Callback<SubResponse>() {

            @Override
            public void onResponse(@NonNull Call<SubResponse> call, @NonNull Response<SubResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String status = response.body().getStatus();
                        if (status.equals("1")) {
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "1");
                            playContent(mediaSource);

                        } else {
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "0");
                            player.stop();
                            //openDialogForSub();
                            openCustomDialog("");

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<SubResponse> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            //  if (playerState)
            initializePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onResume() {
        super.onResume();
        hideSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            //  if (playerState)
            initializePlayer();
        }
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO)))
            sendUserActivity(SharedPref.read(Constant.USER_PHONE_NO), getContentId(), getContentType(), getContentName());

        logForFireBase();

        final String userMobileNo = SharedPref.read(Constant.USER_PHONE_NO);
        final String operator = SharedPref.read(Constant.USER_OPERATOR);


        // int saveTimePre = SharedPref.readInt(getContentId());
        TimeTracker timeTracker = db.getTime(getContentId());
        int saveTime = 0;
        if (timeTracker != null) {
            saveTime = timeTracker.getTime();
        }
        int freePlayTime = 0;
        if (!TextUtils.isEmpty(getFreePlayTime())) {
            freePlayTime = Integer.valueOf(getFreePlayTime());
        }
        /*TODO
         * free play time checking */
        int remainingTime = freePlayTime - saveTime;
        // int timeInSec = remainingTime - saveTime;
        if (remainingTime > 0) {
            waitTimer = new CountDownTimer(remainingTime * 1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    timeCount++;
                    //called every 300 milliseconds, which could be used to
                    //send messages or some other action
                }

                public void onFinish() {

                    int freePlayTime = 0;
                    if (!TextUtils.isEmpty(getFreePlayTime())) {
                        freePlayTime = Integer.valueOf(getFreePlayTime());
                    }

                    timeCount = freePlayTime;
                    // SharedPref.writeInt(getContentId(), timeCount);
                    TimeTracker timeTracker = new TimeTracker(getContentId(), timeCount);
                    db.updateNote(timeTracker);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // registered user

                            if (!TextUtils.isEmpty(isPremium()) && isPremium().equals(Constant.CONTENT_PREMIUM)) {
                                // content premium
                                String mobileNo = SharedPref.read(Constant.USER_PHONE_NO);
                                String operator = SharedPref.read(Constant.USER_OPERATOR);
                                if (!TextUtils.isEmpty(mobileNo) && !TextUtils.isEmpty(operator)
                                        && mobileNo.length() > 12 && operator.length() > 5) {
                                    if (SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS)
                                            .equalsIgnoreCase("1")) {
                                        playbackPosition = player.getCurrentPosition();
                                        SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                                        playContent(mediaSource);
                                    } else {
                                        // already have mobile no and operator so go to sub page
                                        if (player != null)
                                            player.stop();
                                        //openDialogForSub();
                                        openCustomDialog("");
                                       /* startActivity(new Intent(PlayerActivity.this,
                                                SubscriptionActivity.class));*/
                                    }
                                } else {
                                    // check wifi and mobile
                                    player.stop();
                                    getOperator();
                                }

                            } else {
                                // free content
                                playbackPosition = player.getCurrentPosition();
                                SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                                playContent(mediaSource);
                            }
                        }
                    });

                }
            }.start();

        } else {
            int freePTime = 0;
            if (!TextUtils.isEmpty(getFreePlayTime())) {
                freePTime = Integer.valueOf(getFreePlayTime());
            }
            timeCount = freePTime;
            // SharedPref.writeInt(getContentId(), timeCount);
            timeTracker = new TimeTracker(getContentId(), timeCount);
            db.updateNote(timeTracker);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // registered user
                    if (!TextUtils.isEmpty(isPremium()) && isPremium().equals(Constant.CONTENT_PREMIUM)) {
                        // content premium
                        String mobileNo = SharedPref.read(Constant.USER_PHONE_NO);
                        String operator = SharedPref.read(Constant.USER_OPERATOR);
                        if (!TextUtils.isEmpty(mobileNo) && !TextUtils.isEmpty(operator)
                                && mobileNo.length() > 12 && operator.length() > 5) {
                            if (SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS)
                                    .equalsIgnoreCase("1")) {

                                playbackPosition = player.getCurrentPosition();
                                SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                                playContent(mediaSource);
                            } else {
                                // already have mobile no and operator so go to sub page
                                if (player != null)
                                    player.stop();
                                //openDialogForSub();
                                openCustomDialog("");
                                /*startActivity(new Intent(PlayerActivity.this,
                                        SubscriptionActivity.class));*/
                            }
                        } else {
                            // check wifi and mobile
                            player.stop();
                            getOperator();
                        }
                    } else {
                        // free content
                        // SharedPref.write(getContentId() + Constant.PLAY_BACK_POSITION, String.valueOf(playbackPosition));
                        playContent(mediaSource);
                    }
                }
            });

        }

    }


    @Override
    public void onPause() {
        super.onPause();
        //playerState = false;
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TimeTracker timeTracker;
        playbackPosition = player.getCurrentPosition();

        int totalTimeCount = db.getTime(getContentId()).getTime() + timeCount;
        if (Integer.valueOf(getFreePlayTime()) > totalTimeCount) {
            timeTracker = new TimeTracker(getContentId(), totalTimeCount);
            db.updateNote(timeTracker);
        }

        /*else {
            timeTracker = new TimeTracker(getContentId(), playbackTime);
            db.updateNote(timeTracker);
        }*/

        if (waitTimer != null) {
            waitTimer.cancel();
            waitTimer = null;
        }
        releasePlayer();
        this.finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exo_play:
                player.setPlayWhenReady(true);
                playbackPosition = player.getPlaybackState();
                if (player.getPlaybackState() == Player.STATE_ENDED)
                    player.seekTo(0);

                break;
            case R.id.exo_pause:
                player.setPlayWhenReady(false);
                player.getPlaybackState();
                break;
            case R.id.exo_ffwd:
                fastForward();
                break;
            case R.id.exo_rew:
                fastRewind();
                break;

           /* case R.id.exo_setting:
                trackerSelector(v);
                break;*/

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.lyt_back:
                onBackPressed();
                break;
        }

    }

    private void changeOrientation() {
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // code for portrait mode
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            // code for landscape mode
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }


    private void trackerSelector(View v) {
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo != null) {
            int rendererType = mappedTrackInfo.getRendererType(0);

            boolean allowAdaptiveSelections =
                    rendererType == C.TRACK_TYPE_VIDEO
                            || (rendererType == C.TRACK_TYPE_AUDIO
                            && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                            == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);
            Pair<android.app.AlertDialog, TrackSelectionView> dialogPair =
                    TrackSelectionView.getDialog(this, "Video Quality", trackSelector, 0);

            //TrackSelectionArray trackSelectionArray=mappedTrackInfo.getTrackGroups(0);


            dialogPair.first.setInverseBackgroundForced(true);
            dialogPair.second.setShowDisableOption(true);
            dialogPair.second.setAllowAdaptiveSelections(allowAdaptiveSelections);
            dialogPair.first.show();
        }


        PopupMenu popupMenu = new PopupMenu(this, v);
        Menu menu = popupMenu.getMenu();
        menu.add(Menu.NONE, 0, 0, "Video Quality");
        //player.get


        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                DefaultTrackSelector.Parameters parameter = trackSelector.getParameters();
                return false;
            }
        });

    }

    private void customTrackerSelector() {
    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    @SuppressWarnings("ReferenceEquality")
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        if (trackGroups != lastSeenTrackGroupArray) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            if (mappedTrackInfo != null) {
                if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                        == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                    showToast(R.string.error_unsupported_video);
                }
                if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO)
                        == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                    showToast(R.string.error_unsupported_audio);
                }
            }
            lastSeenTrackGroupArray = trackGroups;
        }
    }


    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                progressBar.setVisibility(View.VISIBLE);
                break;

            case Player.STATE_ENDED:
                break;

            case Player.STATE_IDLE:
                break;

            case Player.STATE_READY:
                progressBar.setVisibility(View.GONE);
                break;

            default:
                player.stop();
                break;

        }

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Toast.makeText(this, "Can not play facing problem Please try again"
                , Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onVisibilityChange(int visibility) {

    }


    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void preparePlayback() {

    }

    private void getOperator() {

        switch (NetUtil.getNetworkInfo(this)) {
            case "Mobile":
                getMSISDN();
                break;

            case "Wifi":
                openCustomDialog("Wifi");
                break;
            default:
                break;
        }
    }


    private void getMSISDN() {

        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {
                            return;
                        } else {
                            String operatorName = operatorInfo.getOp();
                            String mobileNo = operatorInfo.getMobileNo();
                            SharedPref.write(Constant.USER_OPERATOR, operatorName);
                            SharedPref.write(Constant.USER_PHONE_NO, mobileNo);
                            isSubscribe(mobileNo, operatorName);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(PlayerActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }


    private void drmCodebase() {
        /*TODO
         * for drm protected video*/

       /* DefaultDrmSessionManager<FrameworkMediaCrypto> drmSessionManager = null;

        String drmLicenseUrl = Constant.DRM_LICENCE_URL;
        int errorStringId = R.string.error_drm_unknown;
        try {

            String DRM_SCHEME_EXTRA = "widevine";

            UUID drmSchemeUuid = Util.getDrmUuid(DRM_SCHEME_EXTRA);

            if (drmSchemeUuid == null) {
                errorStringId = R.string.error_drm_unsupported_scheme;
            } else {
                drmSessionManager =
                        buildDrmSessionManagerV18(
                                drmSchemeUuid, drmLicenseUrl, null, true);
            }
        } catch (UnsupportedDrmException e) {
            errorStringId = e.reason == UnsupportedDrmException.REASON_UNSUPPORTED_SCHEME
                    ? R.string.error_drm_unsupported_scheme : R.string.error_drm_unknown;
        }
        if (drmSessionManager == null) {
            Toast.makeText(this, errorStringId, Toast.LENGTH_LONG).show();
            finish();
            return;
        }


        boolean preferExtensionDecoders = false;
        @DefaultRenderersFactory.ExtensionRendererMode int extensionRendererMode =
                ((CinematicApp) getApplication()).useExtensionRenderers()
                        ? (preferExtensionDecoders ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        DefaultRenderersFactory renderersFactory =
                new DefaultRenderersFactory(this, extensionRendererMode);

        TrackSelection.Factory trackSelectionFactory;
        //trackSelectionFactory = new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

        trackSelectionFactory = new RandomTrackSelection.Factory();

        trackSelector = new DefaultTrackSelector(trackSelectionFactory);


        player =
                ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, drmSessionManager);

        player.addListener(this);


        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getPackageName()));
        Uri uri = Uri.parse(Constant.DRM_PROTECTED_URL);

         player.prepare(mediaSource);*/

        /*TODO
         * for simple media source factory*/

      /*  MediaSource mediaSource = new DashMediaSource.Factory(
                new DefaultDashChunkSource.Factory(dataSourceFactory),
                dataSourceFactory)
                .createMediaSource(uri);

                 player.prepare(mediaSource);
*/

        /*TODO
         * Ready media source*/


        /*TODO
         * ExtraMedia source code */

        /*MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(getUrl()));*/
    }

}
