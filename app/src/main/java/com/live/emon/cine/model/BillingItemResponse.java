package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillingItemResponse {

    @SerializedName("Items")
    @Expose
    private BillingItem billingItem;


    public BillingItem getItems() {
        return billingItem;
    }

    public void setItems(BillingItem items) {
        this.billingItem = items;
    }


}
