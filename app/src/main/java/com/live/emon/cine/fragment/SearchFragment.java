package com.live.emon.cine.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.CategoryAdapter;
import com.live.emon.cine.adapter.HorizontalRecyclerViewAdapter;
import com.live.emon.cine.model.Category;
import com.live.emon.cine.model.CategoryResponse;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.NetUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SearchFragment extends Fragment implements TextWatcher, View.OnClickListener {

    private View view;
    private RecyclerView recyclerView, recyclerViewCategories;
    private HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;
    private CategoryAdapter categoryAdapter;
    private List<Item> items;
    private List<Category> categoryList;
    private Call<ItemResponse> responseCall;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;

    public SearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_search, container, false);
        initToolbar();
        initView();
        initViewRecyclerView();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

       /* Tovuti.from(getActivity()).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });*/

        if (NetUtil.isNetConnected(getActivity())) {
            loadData(true);
        } else {
            loadData(false);
        }
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(mToolbar);



        /*Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setText(getString(R.string.app_name));
        tvToolbarFont.setTypeface(font);*/
    }

    private void initView() {
        ImageButton imageButtonSearch = (ImageButton) view.findViewById(R.id.img_btn_search);
        imageButtonSearch.setOnClickListener(this);
        final EditText editTextSearch = (EditText) view.findViewById(R.id.edt_search);

        relativeLytNoNetwork = (RelativeLayout) view.findViewById(R.id.relative_lyt_no_network);

        imgNoNetLogo = (ImageView) view.findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = (TextView) view.findViewById(R.id.tv_something_wrong);
        tvfailedMessage = (TextView) view.findViewById(R.id.failed_message);

        btnFailedRetry = (Button) view.findViewById(R.id.btn_failed_retry);

        btnFailedRetry.setOnClickListener(this);
        btnFailedRetry.setOnClickListener(this);


        editTextSearch.requestFocus();
        editTextSearch.addTextChangedListener(this);
        // Check if no view has focus:
        View view = Objects.requireNonNull(getActivity()).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);

            //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    private void showKeyBoard() {
        View view = Objects.requireNonNull(getActivity()).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    private void initViewRecyclerView() {
        items = new ArrayList<>();
        categoryList = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        horizontalRecyclerViewAdapter = new HorizontalRecyclerViewAdapter(getActivity(), items);
        recyclerView.setAdapter(horizontalRecyclerViewAdapter); // set the Adapter to RecyclerView
        recyclerView.setFocusable(false);

        recyclerViewCategories = (RecyclerView) view.findViewById(R.id.recycler_view_categories);
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoryAdapter = new CategoryAdapter(getActivity(), categoryList);
        recyclerViewCategories.setAdapter(categoryAdapter);
        // set LayoutManager to RecyclerView


        // recyclerView.setItemAnimator(new DefaultItemAnimator());
       /* recyclerView.addItemDecoration(new MyDividerItemDecoration(2,
                Objects.requireNonNull(getActivity()).getResources().getColor(R.color.border_color)));
*/
    }


    @Override
    public void onPause() {
        super.onPause();
        // Tovuti.from(getActivity()).stop();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerViewCategories.setVisibility(View.GONE);
            searchContent(s.toString().toLowerCase());
        } else {
            recyclerView.setVisibility(View.GONE);
            recyclerViewCategories.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void loadCategory() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<CategoryResponse> callCategory = apiService.getCategories();
        callCategory.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<CategoryResponse> call, @NonNull Response<CategoryResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() == null)
                        return;
                    categoryList = response.body().getCategories();
                    if (categoryList.size() > 0) {
                        categoryAdapter = new CategoryAdapter(getActivity(), categoryList);
                        recyclerViewCategories.setAdapter(categoryAdapter);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryResponse> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }

    private void searchContent(String searchKey) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        responseCall = apiService.searchContent(searchKey);
        responseCall.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() == null)
                        return;
                    List<Item> itemResponses = response.body().getItems();
                    Timber.e(itemResponses.toString());
                    horizontalRecyclerViewAdapter = new HorizontalRecyclerViewAdapter(getActivity(), itemResponses);
                    recyclerView.setAdapter(horizontalRecyclerViewAdapter); // set the Adapter to RecyclerView

                }
            }

            @Override
            public void onFailure(@NonNull Call<ItemResponse> call, @NonNull Throwable t) {

            }
        });
    }

    private void loadData(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);
            loadCategory();
        } else {
            //    Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_search:
                showKeyBoard();
                break;
            case R.id.btn_failed_retry:

                if (NetUtil.isNetConnected(getActivity())) {
                    loadData(true);
                } else {
                    loadData(false);
                }
                break;
        }
    }
}
