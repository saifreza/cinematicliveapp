package com.live.emon.cine.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import android.view.View;
import android.widget.*;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.androidstudy.networkmanager.internal.NetworkUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.live.emon.cine.R;
import com.live.emon.cine.listner.DataListener;
import com.live.emon.cine.listner.ImageManager;
import com.live.emon.cine.model.OperatorInfo;
import com.live.emon.cine.model.SubResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;
import com.live.emon.cine.utils.TelePhoneManagerUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import java.util.Objects;

public class UserBillingActivity extends AppCompatActivity
        implements View.OnClickListener, DataListener {

    private TextView tvCountyCode;
    private LinearLayout lytMain;
    private RelativeLayout relativeLytNoNetwork, relativeLytNoItem;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;
    private Button btnSignUp;
    private Thread thread;
    private volatile boolean running = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_billing);
        ImageManager.getInstance(this).setImageListener(this);
        initView();
        //setCountyCode();


    }

    private void internetChecking(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);

            thread = new Thread() {

                @Override
                public void run() {
                    if (!running) return;
                    getOperator();
                }
            };
            thread.start();

        } else {
            //    Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }

    private void getOperator() {

        switch (NetUtil.getNetworkInfo(this)) {
            case "Mobile":
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO)) &&
                        !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
                    if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDR111")
                            || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDA111")
                            || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDG111")
                            || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDB111"))
                        isSubscribe(SharedPref.read(Constant.USER_PHONE_NO),
                                SharedPref.read(Constant.USER_OPERATOR));
                } else {
                    getMSISDN();
                }

                break;
            case "Wifi":
                if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO)) &&
                        !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
                    isSubscribe(SharedPref.read(Constant.USER_PHONE_NO), SharedPref.read(Constant.USER_OPERATOR));
                }
                break;
            default:

                break;
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_reg, menu);
        return true;
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                internetChecking(isConnected);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Tovuti.from(this).stop();
    }
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

          *//*  case R.id.action_sign_in:
                Intent intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_sign_up:
                Intent intent = new Intent(this, RegistrationActivity.class);
                startActivity(intent);
                return true;
                *//*
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_get_start:
                if (NetworkUtil.isConnected(UserBillingActivity.this)) {
                    relativeLytNoNetwork.setVisibility(View.GONE);
                    //getOperator();
                    startActivity(new Intent(this,
                            MainActivity.class));
                } else {
                    relativeLytNoNetwork.setVisibility(View.VISIBLE);
                    tvfailedMessage.setVisibility(View.VISIBLE);
                    tvSomethingWrong.setVisibility(View.VISIBLE);
                    imgNoNetLogo.setVisibility(View.VISIBLE);
                    btnFailedRetry.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.btn_failed_retry:
                /*Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
                    @Override
                    public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                        internetChecking(isConnected);
                    }
                });*/
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ImageManager.getInstance(this).deactivedListener();
    }

    private void initView() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        TextView tvToolbarFont = findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }

        btnSignUp = findViewById(R.id.btn_get_start);
        lytMain = findViewById(R.id.lyt_main);
        relativeLytNoNetwork = findViewById(R.id.relative_lyt_no_network);
        imgNoNetLogo = findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = findViewById(R.id.tv_something_wrong);
        tvfailedMessage = findViewById(R.id.failed_message);


        relativeLytNoItem = findViewById(R.id.relative_lyt_no_item);
        btnFailedRetry = findViewById(R.id.btn_failed_retry);

       /* tvItemMessage = (TextView) findViewById(R.id.no_item_message);
        tvItemMessage.setOnClickListener(this);
       */

        btnFailedRetry.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);


    }


    private void loadBannerImage() {
        Glide.with(this)
                .load(Constant.REG_URL)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        Drawable drawable = new BitmapDrawable(getResources(), resource);
                        lytMain.setBackground(drawable);

                    }
                });
    }

    private void setCountyCode() {
        String countyCode = TelePhoneManagerUtil.GetCountryZipCode(this);
        if (!TextUtils.isEmpty(countyCode)) {
            String text = getString(R.string.plus) + countyCode;
            tvCountyCode.setText(text);
        }
    }


    public void applyFontForToolbarTitle(Activity context) {
        Toolbar toolbar = context.findViewById(R.id.toolbar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                Typeface titleFont = Typeface.
                        createFromAsset(context.getAssets(), "fonts/28 Days Later");
                if (tv.getText().equals(toolbar.getTitle())) {
                    tv.setTypeface(titleFont);
                    break;
                }
            }
        }
    }

    @Override
    protected void onStop() {
        Tovuti.from(this).stop();
        super.onStop();
    }

    @Override
    public void loadImage(Drawable drawable) {
        if (drawable == null) {
            loadBannerImage();
        } else
            lytMain.setBackground(drawable);
    }

    private void getMSISDN() {

        ApiInterface jsonPostService =
                ApiClient.
                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {
                            return;
                        } else {
                            String operatorName = operatorInfo.getOp();
                            String mobileNo = operatorInfo.getMobileNo();
                            SharedPref.write(Constant.USER_OPERATOR, operatorName);
                            SharedPref.write(Constant.USER_PHONE_NO, mobileNo);
                            isSubscribe(mobileNo, operatorName);
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(UserBillingActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }





    private void isSubscribe(String mobileNo, String op) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<SubResponse> call = jsonPostService.isSubscribe(mobileNo, op);
        call.enqueue(new Callback<SubResponse>() {

            @Override
            public void onResponse(@NonNull Call<SubResponse> call, @NonNull Response<SubResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String status = response.body().getStatus();
                        SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS,
                                status);
                        running = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<SubResponse> call, @NonNull Throwable t) {
                Toast.makeText(UserBillingActivity.this, "No Network Found",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}
