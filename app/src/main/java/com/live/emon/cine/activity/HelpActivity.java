package com.live.emon.cine.activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;

import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;

import com.live.emon.cine.model.HelpResponse;
import com.live.emon.cine.model.ItemHelp;

import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.SharedPref;


import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class HelpActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private TextView tvHelpInfo;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initView();
        initToolbar();

        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                internetChecking(isConnected);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initView() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        int myColor = Color.parseColor("#494848");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.BLACK, Color.GRAY);
        swipeRefresh.setRefreshing(true);
        tvHelpInfo = findViewById(R.id.tv_help_info);
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        if (mToolbar != null) {
            ((AppCompatActivity) this).setSupportActionBar(mToolbar);
        }

        TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }  tvToolbarFont.setTypeface(font);
    }

    private void getHelp(String operator) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<HelpResponse> call = jsonPostService.getHelp(operator.toLowerCase());
        call.enqueue(new Callback<HelpResponse>() {

            @Override
            public void onResponse(@NonNull Call<HelpResponse> call, @NotNull Response<HelpResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        List<ItemHelp> itemHelps = response.body().getItemHelp();
                        if (itemHelps == null)
                            return;
                        String helpInfo = itemHelps.get(0).getHelpInfo();
                        tvHelpInfo.setText(Html.fromHtml(helpInfo));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                swipeRefresh.setRefreshing(false);

            }

            @Override
            public void onFailure(@NonNull Call<HelpResponse> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    public String getOperator() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        assert telephonyManager != null;
        int simState = telephonyManager.getSimState();

        String simOperatorName = "";
        switch (simState) {

            case (TelephonyManager.SIM_STATE_ABSENT):
                break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
                break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_UNKNOWN):
                break;
            case (TelephonyManager.SIM_STATE_READY): {

                // Get the SIM country ISO code
                String simCountry = telephonyManager.getSimCountryIso();

                // Get the name of the SIM operator
                simOperatorName = telephonyManager.getSimOperatorName();

                // Get the SIM’s serial number
                // String simSerial = telephonyManager.getSimSerialNumber();
            }
        }

        return simOperatorName;
    }


    private char getOperatorCharacter() {
        return SharedPref.read(Constant.USER_PHONE_NO).charAt(4);
    }

    private String getOperatorName(char value) {
        String operatorName = "";
        switch (value) {

            case '6':
                operatorName = "airtel";
                break;
            case '7':
                operatorName = "grameenphone";
                break;
            case '8':
                operatorName = "robi";
                break;
            case '9':
                operatorName = "banglalink";
                break;
            default:
                break;
        }

        return operatorName;

    }

    private void internetChecking(boolean isConnected) {
        if (isConnected) {
            getHelp(getOperator());
        } else {
            swipeRefresh.setRefreshing(false);
            Toast.makeText(this, "No Network found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefresh() {

        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                internetChecking(isConnected);
            }
        });

    }
}
