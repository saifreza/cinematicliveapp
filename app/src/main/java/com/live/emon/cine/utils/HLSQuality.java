package com.live.emon.cine.utils;

public enum HLSQuality {
    Auto, Quality1080, Quality720, Quality480, NoValue
}