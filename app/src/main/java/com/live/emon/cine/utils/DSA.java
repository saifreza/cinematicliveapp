package com.live.emon.cine.utils;

import java.security.*;

public class DSA {

    private static final String alg = "DSA";
    private KeyPairGenerator kg = null;

    KeyPair keyPair = kg != null ? kg.genKeyPair() : null;
    byte[] signature;


    {
        try {
            performVerification("test", alg, signature, keyPair.getPublic());
            kg = KeyPairGenerator.getInstance(alg);
            signature = performSigning("test", alg, keyPair);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    static byte[] performSigning(String s, String alg, KeyPair keyPair) throws Exception {
        Signature sign = Signature.getInstance(alg);
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        sign.initSign(privateKey);
        sign.update(s.getBytes());
        return sign.sign();
    }

    static void performVerification(String s, String alg, byte[] signature, PublicKey publicKey)
            throws Exception {
        Signature sign = Signature.getInstance(alg);
        sign.initVerify(publicKey);
        sign.update(s.getBytes());
        System.out.println(sign.verify(signature));
    }

    public static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
