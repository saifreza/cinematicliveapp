package com.live.emon.cine.utils;

public interface Constant {

    String APP_NAME = "BANGLALINK CINEMATIC";
    boolean IS_BANGLALINK = false;

    //  String URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
    //String DASH_URL = "https://d1nxk4x0uff6m5.cloudfront.net/9c87f88d-ed53-4548-86bb-78fab96993da/dash/1min.mpd";
    // String DRM_PROTECTED_URL = "https://storage.googleapis.com/wvmedia/cenc/h264/tears/tears.mpd";
    String DRM_PROTECTED_URL = "https://s3-ap-southeast-1.amazonaws.com/cinematic-vod/boom/stream.mpd";
    String DRM_LICENCE_URL = "https://wv.service.expressplay.com/hms/wv/rights/?ExpressPlayToken=BAAUGMF0KcgAAABg1spmvzD6n4B96V-yC2cNiq2-otKjUHEvhOVP91mn95zC1tVKIm_yVqCaxqwXODD8xIugAUnmFrmZHY1jSi2qZ-WaBWMmzVdCsCmciCH-riZ9DaUiNrJo2QLezr7OeLL8oPUNW-DBFERtQIpooZmQ-66N_cY";
    //String DRM_LICENCE_URL = "https://proxy.uat.widevine.com/proxy?provider=widevine_test";
    String CURRENT_INDEX = "CURRENT_INDEX";
    String CURRENT_POSITION = "CURRENT_POSITION";
    String CURRENT_WINDOW = "CURRENT_WINDOW";
    String CONTENT_URL = "CONTENT_URL";
    String CONTENT_CATEGORY = "CONTENT_CATEGORY";
    String CONTENT_TYPE = "CONTENT_TYPE";
    String CATEGORY_TITLE = "CATEGORY_TITLE";
    String CONTENT_ID = "CONTENT_ID";
    String CONTENT_WATCH_STATE = "CONTENT_WATCH_STATE";
    String CONTENT_FLAG = "CONTENT_FLAG";
    String CONTENT_DETAILS = "CONTENT_DETAILS";
    String REG_URL = "https://live-technologies-vod.akamaized.net/cinematic/assets/img/reg_bg.jpg";
    String CONTENT_PREMIUM_STATUS = "CONTENT_PREMIUM_STATUS";
    String CONTENT_PREMIUM = "1";
    String CONTENT_NAME = "CONTENT_NAME";
    String CONTENT_FREE = "0";
    String CONTENT_FREE_PLAY_TIME = "CONTENT_FREE_PLAY_TIME";
    String USER_NAME = "USER_NAME";
    String USER_OPERATOR = "USER_OPERATOR";
    String USER_PHONE_NO = "USER_PHONE_NO";
    String DEVICE_UNIQUE_ID = "DEVICE_UNIQUE_ID";
    String USER_SING_IN_OR_SIGN_UP = "USER_SING_IN_OR_SIGN_UP";
    String USER_SING_IN_OR_SIGN_UP_STATUS = "USER_SING_IN_OR_SIGN_UP_STATUS";
    String SIGN_IN = "SIGN_IN";
    String SIGN_UP = "SIGN_UP";
    String USER_SUBSCRIPTION_STATUS = "USER_SUBSCRIPTION_STATUS";
    String CONTENT_WATCH_STATUS = "CONTENT_WATCH_STATUS";
    String CLICK_MOBILE_PAYMENT_BUTTON = "CLICK_MOBILE_PAYMENT_BUTTON";
    int CLICK_MOBILE_PAYMENT_BUTTON_COUNT = 0;
    int MY_PERM_REQUEST_SMS_READ = 9001;
    String PLAY_BACK_POSITION = "PLAY_BACK_POSITION";

    String COMING_SOON = "coming soon";
    String RECENTLY_ADDED = "recently added";
    String LIKED = "LIKED";
    String NOT_LIKED = "NOT_LIKED";
    String OTP_NUMBER = "otp_number";
    String CONTENT_SAVE_STATUS = "status";
    String SUB_API = "SUB_API";
    String LOAD_SUB_API = "LOAD_SUB_API";
    String API_MESSAGE = "API_MESSAGE";
    String API_BILLING = "API_BILLING";
    String MESSAGE_BODY = "MESSAGE_BODY";
    String BILLING_AMOUNT = "BILLING_AMOUNT";
    String SERVICE = "SERVICE";
    String MESSAGE = "MESSAGE";
    String MESSAGE_BN = "MESSAGE_BN";
    String MESSAGE_EN = "MESSAGE_EN";
    String LOGO = "LOGO";
    String OPERATOR_NAME = "OPERATOR_NAME";
    String ACTIVITY_TAG = "ACTIVITY_TAG";
    String DIRECT_SUB = "DIRECT_SUB";

}
