package com.live.emon.cine.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.live.emon.cine.R;
import com.live.emon.cine.activity.*;
import com.live.emon.cine.activity.UserBillingActivity;
import com.live.emon.cine.model.BillingItem;
import com.live.emon.cine.model.BillingItemResponse;
import com.live.emon.cine.model.OperatorInfo;
import com.live.emon.cine.model.SubResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import java.util.Objects;

public class MoreFragment extends Fragment implements View.OnClickListener {

    private View view;
    private LinearLayout lytMyList;
    private TextView tvSignOut, tvPrivacyPolicy, tvSub, tvHelp;
    private Intent intent;
    private String unSubApi = "";
    private String apiMessage = "";
    private String apiBilling = "";
    private String messageBody = "";
    private ProgressBar progressBar;
    Dialog dialog;

    public MoreFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_more, container, false);
        initToolbar();
        initView();
        return view;
    }

    private void initView() {

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        lytMyList = view.findViewById(R.id.lytMyList);
        lytMyList.setOnClickListener(this);
        tvSignOut = view.findViewById(R.id.tv_sign_out);
        tvSignOut.setOnClickListener(this);
        tvSub = view.findViewById(R.id.tv_sub);
        tvSub.setOnClickListener(this);
        tvPrivacyPolicy = view.findViewById(R.id.tv_privacy_policy);
        tvPrivacyPolicy.setOnClickListener(this);
        tvHelp = view.findViewById(R.id.tv_help);
        tvHelp.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(SharedPref.read(Constant.USER_SING_IN_OR_SIGN_UP_STATUS))) {
            tvSignOut.setText("Sign In");
        } else {
            tvSignOut.setText("Sign Out");
        }

        String status = SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS);
        if (!TextUtils.isEmpty(status)) {
            if (status.equals("1")) {
                // already subscribed user and he/she can continue
                tvSub.setText(getActivity().getResources().getString(R.string.Unsubscribe));
            } else {
                tvSub.setText(getActivity().getResources().getString(R.string.subscribe));
            }
        }



       /* if (TextUtils.isEmpty(SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS))) {
            if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_PHONE_NO))
                    && !TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
                if (SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDR111")
                        || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDA111")
                        || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDG111")
                        || SharedPref.read(Constant.USER_OPERATOR).equalsIgnoreCase("BDB111"))
                    isSubscribe(SharedPref.read(Constant.USER_PHONE_NO), SharedPref.read(Constant.USER_OPERATOR));
            }
        } else {
            if (status.equals("1")) {
                // already subscribed user and he/she can continue
                tvSub.setText(getActivity().getResources().getString(R.string.Unsubscribe));
            } else {
                tvSub.setText(getActivity().getResources().getString(R.string.subscribe));
            }
        }*/


    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        if (mToolbar != null) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        }

        TextView tvToolbarFont = (TextView) view.findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }

        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }
        tvToolbarFont.setTypeface(font);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.lytMyList:
                intent = new Intent(getActivity(), MyWatchListActivity.class);
                startActivity(intent);
                break;

            case R.id.tv_privacy_policy:
                intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_sign_out:

                if (TextUtils.isEmpty(SharedPref.read(Constant.USER_SING_IN_OR_SIGN_UP_STATUS))) {
                    tvSignOut.setText("Sign In");
                    intent = new Intent(getActivity(), SignInActivity.class);
                    startActivity(intent);
                } else {
                    tvSignOut.setText("Sign Out");
                    SharedPref.write(Constant.USER_PHONE_NO, "");
                    SharedPref.write(Constant.USER_SING_IN_OR_SIGN_UP_STATUS, "");
                    intent = new Intent(getActivity(), UserBillingActivity.class);
                    startActivity(intent);
                }
                break;

            case R.id.tv_sub:

                if (tvSub.getText().toString().equals("Unsubscribe")) {

                    String subStatus = SharedPref.read(Constant.USER_SUBSCRIPTION_STATUS);
                    if (!TextUtils.isEmpty(subStatus) && subStatus.equalsIgnoreCase("1")) {
                        // openDialogForUnSub();
                        startActivity(new Intent(getActivity(), UnsubActivity.class)
                        );

                    }
                } else {
                    getOperator();
                }

                break;

            case R.id.tv_help:
                intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
                break;
        }

    }

    private void openDialogForUnSub() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Warning! Are You Sure to Continue! ");

        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.setProgress(100);
                        getUnSubApi(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));

                    }
                });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void getOperator() {
        String mobileNo = SharedPref.read(Constant.USER_PHONE_NO);
        String operator = SharedPref.read(Constant.USER_OPERATOR);

        switch (NetUtil.getNetworkInfo(getActivity())) {
            case "Mobile":
                if (!TextUtils.isEmpty(
                        mobileNo) && !TextUtils.isEmpty(operator)
                        && mobileNo.length() > 12 && operator.length() > 5) {
                    Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
                    intent.putExtra(Constant.DIRECT_SUB, "DIRECT_SUB");
                    intent.putExtra(Constant.ACTIVITY_TAG, "ACTIVITY_TAG");
                    startActivity(intent);

                } else {
                    getMSISDN();
                }
                break;
            case "Wifi":

                if (!TextUtils.isEmpty(mobileNo
                ) && !TextUtils.isEmpty(operator) && mobileNo.length() > 12
                        && operator.length() > 5) {
                    isSubscribe(SharedPref.read(Constant.USER_PHONE_NO),
                            SharedPref.read(Constant.USER_OPERATOR));
                } else {

                    startActivity(new Intent(getActivity(),
                            RegistrationActivity.class));

                }

                break;
            default:
                break;
        }
    }


    private void getMSISDN() {

        ApiInterface jsonPostService =
                ApiClient.

                        getClient("http://movie.cinemahall.mobi/ROBI/")
                        .create(ApiInterface.class);
        Call<OperatorInfo> call = jsonPostService.sendOperatorInfo();
        call.enqueue(new Callback<OperatorInfo>() {

            @Override
            public void onResponse(@NonNull Call<OperatorInfo> call, @NonNull Response<OperatorInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        OperatorInfo operatorInfo = response.body();
                        if (operatorInfo == null) {
                            return;
                        } else {
                            String operatorName = operatorInfo.getOp();
                            String mobileNo = operatorInfo.getMobileNo();
                            SharedPref.write(Constant.USER_OPERATOR, operatorName);
                            SharedPref.write(Constant.USER_PHONE_NO, mobileNo);

                            isSubscribe(mobileNo, operatorName);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorInfo> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                if (t.getMessage().equalsIgnoreCase("Connection reset")) {
                    Toast.makeText(getActivity(), "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }


    private void isSubscribe(String mobileNo, String op) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<SubResponse> call = jsonPostService.isSubscribe(mobileNo, op);
        call.enqueue(new Callback<SubResponse>() {

            @Override
            public void onResponse(@NonNull Call<SubResponse> call, @NonNull Response<SubResponse> response) {
                try {

                    if (response.isSuccessful()) {

                        assert response.body() != null;
                        String status = response.body().getStatus();
                        if (status.equals("1")) {
                            // already subscribed user and he/she can continue
                            tvSub.setText("Unsubscribe");
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, status);

                        } else {

                            String operatorName = SharedPref.read(Constant.USER_OPERATOR);
                            if (!TextUtils.isEmpty(operatorName) && operatorName.equalsIgnoreCase("BDB111")) {
                              /*  Toast.makeText(getActivity(), "Please Try From Baglalink Mobile Data",
                                        Toast.LENGTH_SHORT).show();*/
                                openBLDialog();
                            } else {
                                Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
                                intent.putExtra(Constant.DIRECT_SUB, "DIRECT_SUB");
                                startActivity(intent);
                                tvSub.setText(getActivity().getResources().getString(R.string.subscribe));

                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<SubResponse> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private char getOperatorCharacter() {
        return SharedPref.read(Constant.USER_PHONE_NO).charAt(4);
    }

    private String getOperatorName(char value) {
        String operatorName = "";
        switch (value) {

            case '6':
                operatorName = "airtel";
                break;
            case '7':
                operatorName = "grameenphone";
                break;
            case '8':
                operatorName = "robi";
                break;
            case '9':
                operatorName = "banglalink";
                break;
            default:
                break;
        }

        return operatorName;

    }


    private void getUnSubApi(String operator, String mobileNo) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<BillingItemResponse> call = jsonPostService.getUnSubInfo(operator, mobileNo, "app");
        call.enqueue(new Callback<BillingItemResponse>() {

            @Override
            public void onResponse(@NonNull Call<BillingItemResponse> call, @NotNull Response<BillingItemResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        BillingItem billingItem = response.body().getItems();
                        if (billingItem == null)
                            return;
                        else {
                            if (!TextUtils.isEmpty(billingItem.getApiunSub())) {
                                unSubApi = billingItem.getApiunSub();
                                apiMessage = billingItem.getApiMessage();
                                messageBody = billingItem.getMessageBody();
                                String baseUrl = unSubApi;
                                progressBar.setProgress(100);
                                apiUnSubscription(baseUrl);
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<BillingItemResponse> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }


    private void openBLDialog() {

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.item_custom_dialog);
        //dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        TextView textEnglish = (TextView) dialog.findViewById(R.id.text_english);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        textEnglish.setText(getActivity().getResources().getString(R.string.sub_bl_message_eng));
        text.setText(getResources().getString(R.string.sub_bl_message_bng));
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (!getActivity().isFinishing()) {
            dialog.show();
        }
    }

    private void apiUnSubscription(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String string = response.body();
                        if (string.equals("1")) {
                            apiMessage();
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "0");
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Unsubscription Complete", Toast.LENGTH_SHORT).show();

                            //btnMobilePayment.setBackgroundColor(R.color.white);
                            // btnMobilePayment.setOnClickListener(null);
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                Toast.makeText(getActivity(), "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
        progressBar.setVisibility(View.GONE);
    }

    private void apiMessage() {
        if (!TextUtils.isEmpty(apiMessage)) {
            String baseUrl = apiMessage;
            apiMessage(baseUrl);
        }
    }

    private void apiBilling() {
        if (!TextUtils.isEmpty(apiBilling)) {
            String baseUrl = apiBilling;
            apiBilling(baseUrl);
        }
    }

    private void apiMessage(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                Toast.makeText(getActivity(), "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void apiBilling(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
            }
        });
    }
}
