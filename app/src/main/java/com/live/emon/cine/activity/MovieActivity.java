package com.live.emon.cine.activity;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.HorizontalRecyclerViewAdapter;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.CaseConverter;
import com.live.emon.cine.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MovieActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView recyclerView;
    private HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;
    private List<Item> items;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        initToolbar();
        initView();
    }

    private void initView() {

        relativeLytNoNetwork = findViewById(R.id.relative_lyt_no_network);

        imgNoNetLogo = findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = findViewById(R.id.tv_something_wrong);
        tvfailedMessage = findViewById(R.id.failed_message);

        btnFailedRetry = findViewById(R.id.btn_failed_retry);
        btnFailedRetry.setOnClickListener(this);


        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        int myColor = Color.parseColor("#494848");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.BLACK, Color.GRAY);
        swipeRefresh.setRefreshing(true);


        ImageButton imgBtnBack = findViewById(R.id.img_btn_back);
        imgBtnBack.setOnClickListener(this);

        items = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        horizontalRecyclerViewAdapter = new HorizontalRecyclerViewAdapter(this, items);
        recyclerView.setAdapter(horizontalRecyclerViewAdapter); // set the Adapter to RecyclerView

    }

    @Override
    protected void onResume() {
        super.onResume();
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });
    }


    private void loadData(boolean isConnected) {
        if (isConnected) {
            relativeLytNoNetwork.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(getCategory())) {
                loadContent(getCategory());
            }
        } else {
            swipeRefresh.setRefreshing(false);
            //   Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            relativeLytNoNetwork.setVisibility(View.VISIBLE);
            tvfailedMessage.setVisibility(View.VISIBLE);
            tvSomethingWrong.setVisibility(View.VISIBLE);
            imgNoNetLogo.setVisibility(View.VISIBLE);
            btnFailedRetry.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private String getCategory() {
        String category = "";
        if (getIntent() != null) {
            category = getIntent().getStringExtra(Constant.CATEGORY_TITLE);
        }
        return category;
    }

    private void initToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        TextView tvToolbarFont = findViewById(R.id.toolbarText);
        /*Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);*/
        if (!TextUtils.isEmpty(getCategory())) {
            tvToolbarFont.setText(CaseConverter.camelCase(getCategory()));
        }
    }

    private void loadContent(String category) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ItemResponse> call = apiService.getItemsByCategory(category);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) return;
                    assert response.body() != null;
                    items = response.body().getItems();
                    horizontalRecyclerViewAdapter = new
                            HorizontalRecyclerViewAdapter(MovieActivity.this, items);
                    recyclerView.setAdapter(horizontalRecyclerViewAdapter);
                }
                swipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(@NonNull Call<ItemResponse> call, @NonNull Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }

    @Override
    public void onRefresh() {
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_back:
                onBackPressed();
                break;

            case R.id.btn_failed_retry:
                Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
                    @Override
                    public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                        loadData(isConnected);
                    }
                });
                break;
            default:
                break;

        }
    }

}

