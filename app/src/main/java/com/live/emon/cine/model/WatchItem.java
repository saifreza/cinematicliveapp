package com.live.emon.cine.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WatchItem extends Base {

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("contentId")
    @Expose
    private String contentId;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("watchState")
    @Expose
    private String watchState;


    @SerializedName("premium")
    @Expose
    private String premium;


    public static final Creator<WatchItem> CREATOR = new Creator<WatchItem>() {
        @Override
        public WatchItem createFromParcel(Parcel in) {
            return new WatchItem(in);
        }

        @Override
        public WatchItem[] newArray(int size) {
            return new WatchItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    private WatchItem(Parcel in) {
        thumbnail = in.readString();
        contentId = in.readString();
        category = in.readString();
        watchState = in.readString();
        premium = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnail);
        dest.writeString(contentId);
        dest.writeString(category);
        dest.writeString(watchState);
        dest.writeString(premium);

    }


    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public String getWatchStatus() {
        return watchState;
    }

    public void setWatchStatus(String watchStatus) {
        this.watchState = watchStatus;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }
}