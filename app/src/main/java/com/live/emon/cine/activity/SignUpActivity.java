package com.live.emon.cine.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;
import com.live.emon.cine.utils.TelePhoneManagerUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtCountyCode, edtUserName, edtPhoneNumber;
    private Button btnSignUpOrSingIn;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();
        internetChecking();
        //setCountyCode();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                if (!isConnected) {
                    Toast.makeText(SignUpActivity.this, "No Network Found",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });*/

    }

    @Override
    protected void onStop() {
        Tovuti.from(this).stop();
        super.onStop();
    }

    private void initView() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        TextView tvToolbarFont = (TextView) findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);
        tvToolbarFont.setText(getString(R.string.app_name));

        edtUserName = (EditText) findViewById(R.id.edt_user_name);
        edtPhoneNumber = (EditText) findViewById(R.id.edt_phone_number);
        // edtCountyCode = (EditText) findViewById(R.id.edt_county_code);
        btnSignUpOrSingIn = (Button) findViewById(R.id.btn_sign_in);
        btnSignUpOrSingIn.setOnClickListener(this);

    }


    private String getContentId() {
        String contentId = "";
        if (getIntent() != null) {
            contentId = getIntent().getStringExtra(Constant.CONTENT_ID);
        }
        return contentId;
    }


    private void internetChecking() {
        if (NetUtil.isNetConnected(this)) {
            //relativeLytNoNetwork.setVisibility(View.GONE);

            switch (NetUtil.getNetworkInfo(this)) {
                case "Mobile":
                    getOperator();
                    break;
                case "Wifi":
                    //getOperator();
                    break;
                default:

                    break;
            }
        } else {
            // relativeLytNoNetwork.setVisibility(View.VISIBLE);
        }
    }


    public String getOperator() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        assert telephonyManager != null;
        int simState = telephonyManager.getSimState();

        String simOperatorName = "";
        switch (simState) {

            case (TelephonyManager.SIM_STATE_ABSENT):
                break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
                break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_UNKNOWN):
                break;
            case (TelephonyManager.SIM_STATE_READY): {

                // Get the SIM country ISO code
                String simCountry = telephonyManager.getSimCountryIso();

                // Get the operator code of the active SIM (MCC + MNC)
                String simOperatorCode = telephonyManager.getSimOperator();

                // Get the name of the SIM operator
                simOperatorName = telephonyManager.getSimOperatorName();

                // Get the SIM’s serial number
                // String simSerial = telephonyManager.getSimSerialNumber();
            }
        }

        return simOperatorName;
    }

    private void setCountyCode() {
        String countyCode = TelePhoneManagerUtil.GetCountryZipCode(this);
        if (!TextUtils.isEmpty(countyCode)) {
            String text = getString(R.string.plus) + countyCode;
            edtCountyCode.setText(text);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in:

                if (!TextUtils.isEmpty(edtUserName.getText().toString()) &&
                        !TextUtils.isEmpty(edtPhoneNumber.getText().toString())) {
                    String userName = edtUserName.getText().toString();
                    String phoneNumber = edtPhoneNumber.getText().toString();
                    String countryCOde = "88";

                    if (userName.length() >= 4 && phoneNumber.length() == 11) {
                        String validNumber = phoneNumber.substring(0, 3);
                        switch (validNumber) {
                            case "017":
                                btnSignUpOrSingIn.setClickable(false);
                                progressBar.setVisibility(View.VISIBLE);

                                Toast.makeText(SignUpActivity.this, "Please Wait For Otp",
                                        Toast.LENGTH_SHORT).show();
                                requestForOtp(userName, countryCOde + phoneNumber);
                                break;
                            case "018":
                                btnSignUpOrSingIn.setClickable(false);
                                progressBar.setVisibility(View.VISIBLE);

                                Toast.makeText(SignUpActivity.this, "Please Wait For Otp",
                                        Toast.LENGTH_SHORT).show();
                                requestForOtp(userName, countryCOde + phoneNumber);
                                break;
                            case "016":
                                btnSignUpOrSingIn.setClickable(false);
                                progressBar.setVisibility(View.VISIBLE);

                                Toast.makeText(SignUpActivity.this, "Please Wait For Otp",
                                        Toast.LENGTH_SHORT).show();
                                requestForOtp(userName, countryCOde + phoneNumber);
                                break;
                            case "019":
                                btnSignUpOrSingIn.setClickable(false);
                                progressBar.setVisibility(View.VISIBLE);

                                Toast.makeText(SignUpActivity.this, "Please Wait For Otp",
                                        Toast.LENGTH_SHORT).show();
                                requestForOtp(userName, countryCOde + phoneNumber);
                                break;
                            case "015":
                                btnSignUpOrSingIn.setClickable(false);
                                progressBar.setVisibility(View.VISIBLE);

                                Toast.makeText(SignUpActivity.this, "Please Wait For Otp",
                                        Toast.LENGTH_SHORT).show();
                                requestForOtp(userName, countryCOde + phoneNumber);
                                break;
                            default:
                                Toast.makeText(SignUpActivity.this, "Insert valid phone number",
                                        Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } else if (userName.length() < 4) {
                        Toast.makeText(this, "User Name Must Be 4 Character", Toast.LENGTH_SHORT).show();
                    } else if (phoneNumber.length() != 11) {
                        Toast.makeText(this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Enter UserName and Password", Toast.LENGTH_SHORT).show();
                }

                ;
                break;

        }
    }

    private void requestForOtp(final String deviceId, final String mobileNo) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendUserInfo(deviceId,
                mobileNo, SharedPref.read(Constant.USER_OPERATOR));
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String responseText = response.body();
                        if (responseText.equals("Accepted")) {

                            Toast.makeText(SignUpActivity.this, "Check Your OTP Number In SMS",
                                    Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignUpActivity.this, OTPActivity.class);
                            intent.putExtra(Constant.USER_PHONE_NO, mobileNo);
                            startActivity(intent);
                        } else {
                            Toast.makeText(SignUpActivity.this, responseText,
                                    Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                btnSignUpOrSingIn.setClickable(true);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<String> call, Throwable t) {
                Timber.e(call.toString());
            }

        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                item.setIcon(R.drawable.ic_left_arrow);
               /* Intent intent = new Intent(this,
                        RegistrationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();*/
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
