package com.live.emon.cine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserActivity {

    @Expose
    @SerializedName("userId")
    private String userId;
    @Expose
    @SerializedName("contentId")
    private String contentId;
    @Expose
    @SerializedName("category")
    private String category;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getViewTime() {
        return category;
    }

    public void setViewTime(String viewTime) {
        this.category = viewTime;
    }

}
