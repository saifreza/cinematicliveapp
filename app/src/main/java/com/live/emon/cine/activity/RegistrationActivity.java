package com.live.emon.cine.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.CustomAdapter;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.SharedPref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private EditText edtPhoneNumber;
    private Button btnRegistration;
    private ProgressBar progressBar;
    private RelativeLayout relativeLytNoNetwork;
    private Button btnFailedRetry;
    private ImageView imgNoNetLogo;
    private TextView tvSomethingWrong, tvfailedMessage;
    private ImageButton imgBtnBack;
    String[] operatorName = {"Select Operator", "Robi", "Airtel", "Banglalink"/*, "Grameenphone",*/};

    int flags[] = {R.drawable.common_full_open_on_phone,
            R.drawable.common_google_signin_btn_icon_light,
            R.drawable.common_full_open_on_phone,
            R.drawable.common_google_signin_btn_icon_light};
    private boolean operatorCheck = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initToolbar();
        initView();
    }

    private void initToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        TextView tvToolbarFont = findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }
        tvToolbarFont.setTypeface(font);
        tvToolbarFont.setTextSize(20);
    }


    private void internetChecking() {

        @SuppressLint("HardwareIds")
        String deviceId = Settings.Secure.
                getString(RegistrationActivity.this.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
        String mobileNo = edtPhoneNumber.getText().toString();

        //01850463753

        if (!TextUtils.isEmpty(deviceId) &&
                !TextUtils.isEmpty(mobileNo) &&
                mobileNo.length() == 11 && operatorCheck) {
            btnRegistration.setClickable(false);
            progressBar.setVisibility(View.VISIBLE);
            String operator = SharedPref.read(Constant.USER_OPERATOR);
            requestForOtp(deviceId, "88" + mobileNo, operator);
        } else if (!operatorCheck) {
            Toast.makeText(this, "Select Your Operator ",
                    Toast.LENGTH_SHORT).show();
            SharedPref.write(Constant.USER_OPERATOR, "");
        } else {
            Toast.makeText(this, "Check mobile Number ",
                    Toast.LENGTH_SHORT).show();
            SharedPref.write(Constant.USER_OPERATOR, "");
        }
    }

    private void initView() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayShowTitleEnabled(false);
            // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        TextView tvToolbarFont = findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");
        tvToolbarFont.setTypeface(font);
        tvToolbarFont.setText(getString(R.string.app_name));

        edtPhoneNumber = findViewById(R.id.edt_phone_number);
        btnRegistration = findViewById(R.id.btn_registration);
        btnRegistration.setOnClickListener(this);

        imgBtnBack = findViewById(R.id.img_btn_back);
        imgBtnBack.setOnClickListener(this);
        relativeLytNoNetwork = findViewById(R.id.relative_lyt_no_network);
        imgNoNetLogo = findViewById(R.id.img_no_net_logo);
        tvSomethingWrong = findViewById(R.id.tv_something_wrong);
        tvfailedMessage = findViewById(R.id.failed_message);
        btnFailedRetry = findViewById(R.id.btn_failed_retry);

        Spinner spin = findViewById(R.id.simpleSpinner);
        spin.setOnItemSelectedListener(this);
        // spin.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        CustomAdapter customAdapter = new CustomAdapter(this, flags, operatorName);
        spin.setAdapter(customAdapter);
        relativeLytNoNetwork.setVisibility(View.GONE);

    }


    private void requestForOtp(final String deviceId, final String mobileNo, String operator) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = jsonPostService.sendUserInfo(deviceId,
                mobileNo, operator);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    if (response.code() == 200) {
                        Intent intent = new Intent(RegistrationActivity.this, OTPActivity.class);
                        intent.putExtra(Constant.USER_PHONE_NO, mobileNo);
                        intent.putExtra(Constant.CONTENT_ID, getContentId());
                        startActivity(intent);
                        btnRegistration.setClickable(true);
                        progressBar.setVisibility(View.GONE);
                    }

                    if (response.isSuccessful()) {
                        assert response.body() != null;


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                btnRegistration.setClickable(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(RegistrationActivity.this, "Something went wrong",
                        Toast.LENGTH_SHORT).show();
            }

        });


    }

    private String getContentId() {
        String url = "";
        Intent intent = getIntent();
        if (intent != null)
            url = intent.getStringExtra(Constant.CONTENT_ID);
        return url;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_registration:
                //01850463753

                internetChecking();
                break;

            case R.id.img_btn_back:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (operatorName[position]) {
            case "Robi":
                operatorCheck = true;
                SharedPref.write(Constant.USER_OPERATOR, "BDR111");
                break;
            case "Airtel":
                operatorCheck = true;
                SharedPref.write(Constant.USER_OPERATOR, "BDR111");
                break;
           /* case "Grameenphone":
                operatorCheck = true;
                SharedPref.write(Constant.USER_OPERATOR, "BDG111");
                break;*/
            case "Banglalink":
                operatorCheck = true;
                SharedPref.write(Constant.USER_OPERATOR, "BDB111");
                break;
            default:
                operatorCheck = false;
                //SharedPref.write(Constant.USER_OPERATOR, "BDR111");
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
