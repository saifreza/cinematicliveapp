/*
package com.live.emon.cine.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

public class SQLiteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "Cinematic";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_CATEGORY_TABLE = "CREATE TABLE category ( " +
                "categoryId TEXT PRIMARY KEY, " +
                "category TEXT, " +
                "status INTEGER )";

        // create books table
        db.execSQL(CREATE_CATEGORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS category");

        // create fresh books table
        this.onCreate(db);
    }


    private static final String TABLE_CATEGORY = "category";


    private static final String KEY_CATEGORY_ID = "categoryId";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_CATEGORY_STATUS = "categoryStatus";

    private static final String[] COLUMNS = {KEY_CATEGORY_ID, KEY_CATEGORY, KEY_CATEGORY_STATUS};

    public void addCategory(CategoryNew categoryNew) {
        Log.d("addCategory", categoryNew.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_ID, categoryNew.getCatId());
        values.put(KEY_CATEGORY, categoryNew.getCategory());
        values.put(KEY_CATEGORY_STATUS, categoryNew.getStatus());
        // 3. insert
        db.insert(TABLE_CATEGORY, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public CategoryNew getCategory(int categoryId) {

        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor =
                db.query(TABLE_CATEGORY, // a. table
                        COLUMNS, // b. column names
                        " categoryId = ?", // c. selections
                        new String[]{String.valueOf(categoryId)}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        // 3. if we got results get the first one
        if (cursor != null)
            cursor.moveToFirst();

        // 4. build categoryNew object
        CategoryNew categoryNew = new CategoryNew();
        assert cursor != null;
        categoryNew.setCatId(cursor.getString(0));
        categoryNew.setCategory(cursor.getString(1));
        categoryNew.setStatus(cursor.getInt(2));

        Log.d("getCategory(" + categoryId + ")", categoryNew.toString());

        // 5. return categoryNew
        return categoryNew;
    }

    // Get All Books
    public List<CategoryNew> getAllCategories() {
        List<CategoryNew> categoryNews = new ArrayList<>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_CATEGORY;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        CategoryNew categoryNew = null;
        if (cursor.moveToFirst()) {
            do {
                categoryNew = new CategoryNew();
                categoryNew.setCatId(cursor.getString(0));
                categoryNew.setCategory(cursor.getString(1));
                categoryNew.setStatus(cursor.getInt(2));

                // Add book to books
                categoryNews.add(categoryNew);
            } while (cursor.moveToNext());
        }

        Log.d("getAllCategories()", categoryNews.toString());

        // return books
        return categoryNews;
    }

    // Updating single book
    public int updateCategory(CategoryNew categoryNew) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("category", categoryNew.getCategory()); // get title
        values.put("categoryId", categoryNew.getCatId()); // get title
        values.put("categoryStatus", categoryNew.getStatus()); // get author

        // 3. updating row
        int i = db.update(TABLE_CATEGORY, //table
                values, // column/value
                KEY_CATEGORY_ID + " = ?", // selections
                new String[]{String.valueOf(categoryNew.getCatId())}); //selection args

        // 4. close
        db.close();

        return i;

    }

    // Deleting single book
    public void deleteCategory(CategoryNew categoryNew) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. delete
        db.delete(TABLE_CATEGORY,
                KEY_CATEGORY_ID + " = ?",
                new String[]{String.valueOf(categoryNew.getCatId())});

        // 3. close
        db.close();

        Log.d("deleteCategory", categoryNew.toString());

    }

}
*/
