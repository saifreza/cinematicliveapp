package com.live.emon.cine.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.live.emon.cine.R;
import com.live.emon.cine.adapter.HorizontalRecyclerViewAdapter;
import com.live.emon.cine.adapter.UpComingMoviesAdapter;
import com.live.emon.cine.helper.MyDividerItemDecoration;
import com.live.emon.cine.model.BillingItem;
import com.live.emon.cine.model.BillingItemResponse;
import com.live.emon.cine.model.Item;
import com.live.emon.cine.model.ItemResponse;
import com.live.emon.cine.network.ApiClient;
import com.live.emon.cine.network.ApiInterface;
import com.live.emon.cine.utils.Constant;
import com.live.emon.cine.utils.NetUtil;
import com.live.emon.cine.utils.SharedPref;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UnsubActivity extends AppCompatActivity implements View.OnClickListener {

    private String unSubApi = "";
    private String apiMessage = "";
    private ProgressBar progressBar;
    private Button btnUnSub;
    private ImageButton imgBtnBack;
    private List<Item> items = new ArrayList<>();
    private RecyclerView recyclerView;
    private HorizontalRecyclerViewAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unsub);
        initToolbar();
        initView();
    }

    private void initView() {

        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        btnUnSub = findViewById(R.id.btn_unSubscribe);
        btnUnSub.setOnClickListener(this);
        imgBtnBack = findViewById(R.id.img_btn_back);
        imgBtnBack.setOnClickListener(this);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_coming_soon);
        mAdapter = new HorizontalRecyclerViewAdapter(this, items);
        RecyclerView.LayoutManager mLayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onResume() {
        super.onResume();
     /*   Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                loadData(isConnected);
            }
        });*/

        if (NetUtil.isNetConnected(this)) {
            loadData(true);
        } else {
            loadData(false);
        }

    }

    private void loadData(boolean isConnected) {
        if (isConnected) {
            recyclerView.setVisibility(View.VISIBLE);
            loadUpComingMovie(Constant.COMING_SOON);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }


    private void loadUpComingMovie(String categoryName) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ItemResponse> call = apiService.getItemsByCategory(categoryName);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<ItemResponse> call, @NonNull Response<ItemResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    List<Item> movies = response.body().getItems();
                    if (movies != null) {
                        //  mAdapter.clear();
                        items.addAll(movies);
                        mAdapter.notifyDataSetChanged();
                    }
                    Timber.v("Movies " + movies.toString());
                }


                //Log.d(TAG, "Number of Item Recived: " + movies.size());
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {
                // Log error here since request failed
                // Timber.e(t.toString());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Tovuti.from(this).stop();
    }

    private void initToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        TextView tvToolbarFont = findViewById(R.id.toolbarText);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/28 Days Later.ttf");
        if (!TextUtils.isEmpty(SharedPref.read(Constant.USER_OPERATOR))) {
            if (SharedPref.read(Constant.USER_OPERATOR).equals("BDB111"))
                tvToolbarFont.setText(getString(R.string.app_name_banglalink));
            else {
                tvToolbarFont.setText(getString(R.string.app_name));
            }
        } else {
            tvToolbarFont.setText(getString(R.string.app_name));
        }
        tvToolbarFont.setTypeface(font);
        tvToolbarFont.setTextSize(20);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_unSubscribe:
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(100);
                getUnSubApi(SharedPref.read(Constant.USER_OPERATOR), SharedPref.read(Constant.USER_PHONE_NO));
                break;

            case R.id.img_btn_back:
                onBackPressed();
                break;

           /* case R.id.btn_failed_retry:
                if (NetUtil.isInternetAvailable()) {
                    loadData(true);
                } else {
                    loadData(false);
                }
                break;*/
        }
    }

    private void getUnSubApi(String operator, String mobileNo) {
        ApiInterface jsonPostService = ApiClient.getClient().create(ApiInterface.class);
        Call<BillingItemResponse> call = jsonPostService.getUnSubInfo(operator, mobileNo, "app");
        call.enqueue(new Callback<BillingItemResponse>() {

            @Override
            public void onResponse(@NonNull Call<BillingItemResponse> call, @NotNull Response<BillingItemResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        BillingItem billingItem = response.body().getItems();
                        if (billingItem == null)
                            return;
                        else {
                            if (!TextUtils.isEmpty(billingItem.getApiunSub())) {
                                unSubApi = billingItem.getApiunSub();
                                apiMessage = billingItem.getApiMessage();
                                String baseUrl = unSubApi;
                                progressBar.setProgress(100);
                                apiUnSubscription(baseUrl);
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<BillingItemResponse> call, @NonNull Throwable t) {
                Timber.e(call.toString());
            }

        });
    }

    private void apiUnSubscription(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        String string = response.body();
                        if (string.equals("1")) {
                            apiMessage();
                            SharedPref.write(Constant.USER_SUBSCRIPTION_STATUS, "0");
                            Intent intent = new Intent(UnsubActivity.this, MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(UnsubActivity.this, "Unsubscription Complete", Toast.LENGTH_SHORT).show();

                            //btnMobilePayment.setBackgroundColor(R.color.white);
                            // btnMobilePayment.setOnClickListener(null);
                        } else {
                            //Toast.makeText(UnsubActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                     Toast.makeText(UnsubActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
                Toast.makeText(UnsubActivity.this, "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void apiMessage() {
        if (!TextUtils.isEmpty(apiMessage)) {
            String baseUrl = apiMessage;
            apiMessage(baseUrl);
        }
    }

    private void apiMessage(String url) {
        ApiInterface jsonPostService = ApiClient.getRetrofitClient("").create(ApiInterface.class);
        Call<String> call = jsonPostService.userSubscription(url);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NotNull Response<String> response) {
                try {
                    if (response.isSuccessful()) {
                        // Toast.makeText(UnsubActivity.this, "SSSS", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                   // Toast.makeText(UnsubActivity.this, "Something went wrong try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Timber.e(call.toString());
               // Toast.makeText(UnsubActivity.this, "Something went wrong check network connection", Toast.LENGTH_SHORT).show();
            }
        });
        progressBar.setVisibility(View.GONE);
    }

}
