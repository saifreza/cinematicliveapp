package com.live.emon.cine.listner;

import android.graphics.drawable.Drawable;

public interface DataListener {
    void loadImage(Drawable drawable);
}
